#!/bin/sh

cd macos
curl --fail https://git.openprivacy.ca/openprivacy/buildfiles/raw/branch/master/tor/tor-0.4.8.14-macos.tar.gz --output tor.tar.gz
tar -xzf tor.tar.gz
chmod a+x Tor/tor
cd ..
