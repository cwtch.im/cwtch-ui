import 'package:cwtch/config.dart';
import 'package:cwtch/third_party/linkify/linkify.dart';
import 'package:cwtch/third_party/linkify/uri.dart';
import 'package:test/test.dart';


void main() {
  group('Issue 836: code formatting should take precedence over ClickableLink URL', () {
    test('when formatting and ClickableLink experiment are enabled, format URL in code block as code', () {
      final text = '`my code is https://gentoo.org`';
      final elements = linkify(text, options: LinkifyOptions(messageFormatting: true, parseLinks: true));
      expect(elements.length, 1);
      expect(elements[0].runtimeType, CodeElement);
      expect(elements[0].text, text);
    });
    test('when formatting and ClickableLink experiment are enabled, format bare URL as URL', () {
      final text = 'https://gentoo.org';
      final elements = linkify(text, options: LinkifyOptions(messageFormatting: true, parseLinks: true));
      expect(elements.length, 1);
      expect(elements[0].runtimeType, UrlElement);
      expect(elements[0].text, text);
    });
    test('when formatting is enabled and ClickableLink experiment is disabled, format URL in code block as code', () {
      final text = '`my code is https://gentoo.org`';
      final elements = linkify(text, options: LinkifyOptions(messageFormatting: true, parseLinks: false));
      expect(elements.length, 1);
      expect(elements[0].runtimeType, CodeElement);
      expect(elements[0].text, text);
    });
    test('when formatting is enabled and ClickableLink experiment is disabled, format bare URL as text', () {
      final text = 'https://gentoo.org';
      final elements = linkify(text, options: LinkifyOptions(messageFormatting: true, parseLinks: false));
      expect(elements.length, 1);
      expect(elements[0].runtimeType, TextElement);
      expect(elements[0].text, text);
    });
    test('when formatting is disabled and ClickableLink experiment is enabled, format URL in code block as URL', () {
      final text = '`my code is https://gentoo.org`';
      final elements = linkify(text, options: LinkifyOptions(messageFormatting: false, parseLinks: true));
      print(elements[0].toString());
      print(elements[1].toString());
      expect(elements.length, 3);
      expect(elements[0].runtimeType, TextElement);
      expect(elements[1].runtimeType, UrlElement);
      expect(elements[2].runtimeType, TextElement);
      expect(elements[0].text, '`my code is ');
      expect(elements[1].text, 'https://gentoo.org');
      expect(elements[2].text, '`');
    }, skip: 'Parser URL regex is promiscuous and absorbs trailing tick, and yet this does not seem to impact downstream');
    test('when formatting is disabled and ClickableLink experiment is enabled, format bare URL as URL', () {
      final text = 'https://gentoo.org';
      final elements = linkify(text, options: LinkifyOptions(messageFormatting: false, parseLinks: true));
      expect(elements.length, 1);
      expect(elements[0].runtimeType, UrlElement);
      expect(elements[0].text, text);
    });
    test('when formatting and ClickableLink experiment are both disabled, format URL in code block as text', () {
      final text = '`my code is https://gentoo.org`';
      final elements = linkify(text, options: LinkifyOptions(messageFormatting: false, parseLinks: false));
      expect(elements.length, 1);
      expect(elements[0].runtimeType, TextElement);
      expect(elements[0].text, text);
    });
    test('when formatting and ClickableLink experiment are both disabled, format bare URL as text', () {
      final text = 'https://gentoo.org';
      final elements = linkify(text, options: LinkifyOptions(messageFormatting: false, parseLinks: false));
      expect(elements.length, 1);
      expect(elements[0].runtimeType, TextElement);
      expect(elements[0].text, text);
    });
  });
}
