import 'dart:convert';

import 'package:cwtch/config.dart';
import 'package:cwtch/views/filesharingview.dart';
import 'package:test/test.dart';
import 'package:flutter_gen/gen_l10n/app_localizations_en.dart';

const today = "today";
const yesterday = "yesterday";
const thisWeek = "thisWeek";
const lastWeek = "lastWeek";
const thisMonth = "thisMonth";
const lastMonth = "lastMonth";
const thisYear = "thisYear";
const earlier = "earlier";

class localizer extends AppLocalizationsEn {
  localizer() : super();
  @override
  String get relativeTimeToday => today;
  @override
  String get relativeTimeYesterday => yesterday;
  @override
  String get relativeTimeThisWeek => thisWeek;
  @override
  String get relativeTimeLastWeek => lastWeek;
  @override
  String get relativeTimeThisMonth => thisMonth;
  @override
  String get relativeTimeLastMonth => lastMonth;
  @override
  String get relativeTimeThisYear => thisYear;
  @override
  String get relativeTimeEarlier => earlier;
}

void main() {
  group('Issue 589: Add date headers to list of files', (){
    final datePool = '[{"DateShared": "2024-10-01 11:00:00"}, {"DateShared": "2024-10-01 09:00:00"}, {"DateShared": "2024-09-30 12:00:00"}, {"DateShared": "2024-09-25 12:00:00"}, {"DateShared": "2024-09-23 11:00:00"}, {"DateShared": "2024-09-21 23:00:59"}, {"DateShared": "2024-09-01 14:20:33"}, {"DateShared": "2024-08-20 11:11:11"}, {"DateShared": "2024-07-11 10:10:10"}, {"DateShared": "2024-01-01 01:01:01"}, {"DateShared": "2023-12-31 12:12:12"}, {"DateShared": "2023-05-01 12:10:01"}, {"DateShared": "2023-04-29 11:12:13"}]';
    final List<dynamic> sharedFiles = jsonDecode(datePool as String);
    
    test('detect timestamp on same day', () {
      final time1 = DateTime.parse(sharedFiles[1]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[0]["DateShared"]);
      expect(isToday(time1, time2), true);
      expect(isYesterday(time1, time2), false);
      expect(isThisWeek(time1, time2), true);
      expect(isLastWeek(time1, time2), false);
      expect(isThisMonth(time1, time2), true);
      expect(isLastMonth(time1, time2), false);
      expect(isThisYear(time1, time2), true);
      expect(isEarlierThanThisYear(time1, time2), false);
    });
    test('detect timestamp yesterday', () {
      final time1 = DateTime.parse(sharedFiles[2]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[0]["DateShared"]);
      expect(isToday(time1, time2), false);
      expect(isYesterday(time1, time2), true);
      expect(isThisWeek(time1, time2), true);
      expect(isLastWeek(time1, time2), false);
      expect(isThisMonth(time1, time2), false);
      expect(isLastMonth(time1, time2), true);
      expect(isThisYear(time1, time2), true);
      expect(isEarlierThanThisYear(time1, time2), false);
    });
    test('detect timestamp yesterday when today is january 01', () {
      final time1 = DateTime.parse(sharedFiles[10]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[9]["DateShared"]);
      expect(isToday(time1, time2), false);
      expect(isYesterday(time1, time2), true);
      expect(isThisWeek(time1, time2), false);
      expect(isLastWeek(time1, time2), true);
      expect(isThisMonth(time1, time2), false);
      expect(isLastMonth(time1, time2), true);
      expect(isThisYear(time1, time2), false);
      expect(isEarlierThanThisYear(time1, time2), true);
    });
    test('detect timestamp earlier this week', () {
      final time1 = DateTime.parse(sharedFiles[4]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[3]["DateShared"]);
      expect(isToday(time1, time2), false);
      expect(isYesterday(time1, time2), false);
      expect(isThisWeek(time1, time2), true);
      expect(isLastWeek(time1, time2), false);
      expect(isThisMonth(time1, time2), true);
      expect(isLastMonth(time1, time2), false);
      expect(isThisYear(time1, time2), true);
      expect(isEarlierThanThisYear(time1, time2), false);
    });
    test('detect timestamp last week in same month', () {
      final time1 = DateTime.parse(sharedFiles[5]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[3]["DateShared"]);
      expect(isToday(time1, time2), false);
      expect(isYesterday(time1, time2), false);
      expect(isThisWeek(time1, time2), false);
      expect(isLastWeek(time1, time2), true);
      expect(isThisMonth(time1, time2), true);
      expect(isLastMonth(time1, time2), false);
      expect(isThisYear(time1, time2), true);
      expect(isEarlierThanThisYear(time1, time2), false);
    });
    test('detect timestamp earlier this month', () {
      final time1 = DateTime.parse(sharedFiles[6]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[3]["DateShared"]);
      expect(isToday(time1, time2), false);
      expect(isYesterday(time1, time2), false);
      expect(isThisWeek(time1, time2), false);
      expect(isLastWeek(time1, time2), false);
      expect(isThisMonth(time1, time2), true);
      expect(isLastMonth(time1, time2), false);
      expect(isThisYear(time1, time2), true);
      expect(isEarlierThanThisYear(time1, time2), false);
    });
    test('detect timestamp last week in different month', () {
      final time1 = DateTime.parse(sharedFiles[12]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[11]["DateShared"]);
      expect(isToday(time1, time2), false);
      expect(isYesterday(time1, time2), false);
      expect(isThisWeek(time1, time2), false);
      expect(isLastWeek(time1, time2), true);
      expect(isThisMonth(time1, time2), false);
      expect(isLastMonth(time1, time2), true);
      expect(isThisYear(time1, time2), true);
      expect(isEarlierThanThisYear(time1, time2), false);
    });
    test('detect timestamp last month', () {
      final time1 = DateTime.parse(sharedFiles[8]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[7]["DateShared"]);
      expect(isToday(time1, time2), false);
      expect(isYesterday(time1, time2), false);
      expect(isThisWeek(time1, time2), false);
      expect(isLastWeek(time1, time2), false);
      expect(isThisMonth(time1, time2), false);
      expect(isLastMonth(time1, time2), true);
      expect(isThisYear(time1, time2), true);
      expect(isEarlierThanThisYear(time1, time2), false);
    });
    test('detect timestamp earlier this year', () {
      final time1 = DateTime.parse(sharedFiles[8]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[0]["DateShared"]);
      expect(isToday(time1, time2), false);
      expect(isYesterday(time1, time2), false);
      expect(isThisWeek(time1, time2), false);
      expect(isLastWeek(time1, time2), false);
      expect(isThisMonth(time1, time2), false);
      expect(isLastMonth(time1, time2), false);
      expect(isThisYear(time1, time2), true);
      expect(isEarlierThanThisYear(time1, time2), false);
    });
    test('detect timestamp in earlier year', () {
      final time1 = DateTime.parse(sharedFiles[12]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[0]["DateShared"]);
      expect(isToday(time1, time2), false);
      expect(isYesterday(time1, time2), false);
      expect(isThisWeek(time1, time2), false);
      expect(isLastWeek(time1, time2), false);
      expect(isThisMonth(time1, time2), false);
      expect(isLastMonth(time1, time2), false);
      expect(isThisYear(time1, time2), false);
      expect(isEarlierThanThisYear(time1, time2), true);
    });

    test('assign relative time on same day', () {
      final time1 = DateTime.parse(sharedFiles[1]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[0]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.Today);
    });
    test('assign relative time yesterday', () {
      final time1 = DateTime.parse(sharedFiles[2]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[0]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.Yesterday);
    });
    test('assign relative time yesterday when today is january 01', () {
      final time1 = DateTime.parse(sharedFiles[10]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[9]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.Yesterday);
    });
    test('assign relative time earlier this week', () {
      final time1 = DateTime.parse(sharedFiles[4]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[3]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.ThisWeek);
    });
    test('assign relative time last week in same month', () {
      final time1 = DateTime.parse(sharedFiles[5]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[3]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.LastWeek);
    });
    test('assign relative time earlier this month', () {
      final time1 = DateTime.parse(sharedFiles[6]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[3]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.ThisMonth);
    });
    test('assign relative time last week in different month', () {
      final time1 = DateTime.parse(sharedFiles[12]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[11]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.LastWeek);
    });
    test('assign relative time last month', () {
      final time1 = DateTime.parse(sharedFiles[8]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[7]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.LastMonth);
    });
    test('assign relative time earlier this year', () {
      final time1 = DateTime.parse(sharedFiles[8]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[0]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.ThisYear);
    });
    test('assign relative time in earlier year', () {
      final time1 = DateTime.parse(sharedFiles[12]["DateShared"]);
      final time2 = DateTime.parse(sharedFiles[0]["DateShared"]);
      expect(assignRelativeTime(time1, time2), FileRelativeTime.Earlier);
    });

    test('inject correct time headers in list of dates', () {
      final loc = localizer();
      final observed = injectTimeHeaders(loc, new List.from(sharedFiles.reversed), DateTime.parse("2024-10-01 12:00:00"));
      final expected = [sharedFiles[12],
                        sharedFiles[11],
                        sharedFiles[10],
                        {"TimeHeader": earlier}, 
                        sharedFiles[9],
                        sharedFiles[8],
                        sharedFiles[7],
                        {"TimeHeader": thisYear},
                        sharedFiles[6],
                        sharedFiles[5],
                        {"TimeHeader": lastMonth},
                        sharedFiles[4],
                        sharedFiles[3],
                        {"TimeHeader": lastWeek},
                        sharedFiles[2],
                        {"TimeHeader": yesterday},
                        sharedFiles[1],
                        sharedFiles[0],
                        {"TimeHeader": today}];
      expect(observed, expected);
    });
  });
}
