import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:cwtch/widgets/textfield.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'profile.dart';
import 'contact.dart';
import 'contactlist.dart';

class SearchResult {
  String searchID;
  int conversationIdentifier;
  int messageIndex;
  SearchResult({required this.searchID, required this.conversationIdentifier, required this.messageIndex});
}

class SearchState extends ChangeNotifier {
  bool _searchActive = false;
  int? _selectedSearchMessage;
  String _filter = "";
  String activeSearchID = "";
  List<SearchResult> activeSearchResults = List.empty(growable: true);

  SearchState();

  void newSearch(String searchID) {
    this.activeSearchID = searchID;
    this.activeSearchResults.clear();
    notifyListeners();
  }

  void handleSearchResult(String searchID, int conversationIdentifier, int messageIndex) {
    if (searchID == this.activeSearchID) {
      this.activeSearchResults.add(SearchResult(searchID: searchID, conversationIdentifier: conversationIdentifier, messageIndex: messageIndex));
      notifyListeners();
    }
  }

  int? get selectedSearchMessage => _selectedSearchMessage;
  set selectedSearchMessage(int? newVal) {
    this._selectedSearchMessage = newVal;
    notifyListeners();
  }

  bool get isFiltered => _filter != "";
  String get filter => _filter;
  set filter(String newVal) {
    _filter = newVal.toLowerCase();
    notifyListeners();
  }

  bool get searchActive => _searchActive;
  set searchActive(bool newVal) {
    this._searchActive = newVal;
    notifyListeners();
  }

  bool get searchOrFiltered {
    return this._filter != "" || this._searchActive == true;
  }

  void toggleSearch() {
    if (this.searchActive) {
      this.clearSearch();
    } else {
      this._filter = "";
      this.searchActive = true;
      notifyListeners();
    }
  }

  void clearSearch() {
    this._searchActive = false;
    this._selectedSearchMessage = null;
    this._filter = "";
    this.newSearch("");
    notifyListeners();
  }
}
