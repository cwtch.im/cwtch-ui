{
    "@@locale": "ro",
    "@@last_modified": "2024-06-23T20:32:41+02:00",
    "contactDisconnectSummary": "Disconnect from Contact",
    "messageHistoryEndOfHistory": "Beginning of Conversation History",
    "messageHistoryLoadOlderMessages": "Load Older Messages",
    "relativeTimeEarlier": "Earlier",
    "relativeTimeThisYear": "This Year",
    "relativeTimeLastMonth": "Last Month",
    "relativeTimeThisMonth": "This Month",
    "relativeTimeLastWeek": "Last Week",
    "relativeTimeThisWeek": "This Week",
    "relativeTimeYesterday": "Yesterday",
    "relativeTimeToday": "Today",
    "privateNameHint": "Private name for this account (never shared)",
    "privateNameLabel": "Private Name",
    "settingsExperimentsShowPerformanceDescription": "Display an overlay graph of render time.",
    "settingsExperimentsShowPerformanceTitle": "Show Performance Overlay",
    "settingsImportThemeButton": "Import Theme",
    "settingsImportThemeDescription": "Select theme directory to import for use in Cwtch",
    "settingsImportThemeTitle": "Import Theme",
    "settingsThemeErrorInvalid": "Error: Could not import $themeName, theme.yml missing, not a theme directory",
    "settingThemeOverwriteQuestion": "Theme $themeName already exists, confirm overwrite?",
    "settingThemeOverwriteConfirm": "Confirm",
    "settingsThemeImagesDescription": "Enable display of images from themes",
    "settingsThemeImages": "Theme Images",
    "settingsGroupAbout": "About",
    "localeUzbek": "Uzbek  \/ Oʻzbekcha",
    "profileOfflineAtStart": "Appear Offline at Startup",
    "now": "Now",
    "xSecondsAgo": "$seconds seconds ago",
    "xMinutesAgo": "$minutes minutes ago",
    "xHoursAgo": "$hours hours ago",
    "xDaysAgo": "$days days ago",
    "profileAllowUnknownContacts": "Allow Unknown Contacts",
    "profileBlockUnknownContacts": "Block Unknown Contacts",
    "profileDisableProfile": "Disable Profile",
    "profileEnableProfile": "Enable Profile",
    "profileAppearOnline": "Appear Online",
    "contactDisconnect": "Disconnect  from Contact (if you do not have Appear Offline set this contact may still be able to reestablish a connection to you)",
    "profileAppearOfflineDescription": "By default, when  Cwtch profile is enabled it automatically attempts to connect to know contacts, and allows inbound connections. This settings disables those actions and allows you to choose, manually, which contacts to connect to.",
    "profileAppearOffline": "Appear Offline",
    "deleteServerConfirmBtn": "Sigur doriți sa ștergeți serverul",
    "cannotDeleteServerIfActiveGroups": "There are active groups associated with this Cwtch Server. Please delete them prior to deleting this Cwtch Server entry.",
    "groupsOnThisServerLabel": "Grupurile în care mă aflu care sunt găzduite pe acest server",
    "serverinfoNoGroupInfo": "There are no groups associated with this Cwtch Server.",
    "preserveHistorySettingDescription": "By default, Cwtch will purge conversation history when Cwtch is shutdown. If this setting is enabled, Cwtch will preserve the conversation history of peer conversations.",
    "defaultPreserveHistorySetting": "Preserve Conversation History",
    "localeUk": "Ukrainian \/ українською",
    "profileEnabledDescription": "Activate or Deactivate the profile.",
    "localeSw": "Swahili \/ Kiswahili",
    "localeSv": "Swedish \/ Svenska",
    "fontScalingDescription": "Adjust the relative font scaling factor applied to text and widgets.",
    "defaultScalingText": "Dimensiunea implicită a textului (factor de scară:",
    "localeJa": "Japanese \/ 日本語",
    "retryConnectionTooltip": "Cwtch retries peers regularly, but you can tell Cwtch to try sooner by pushing this button.",
    "retryConnection": "Retry",
    "availabilityStatusTooltip": "Set your availability status",
    "profileInfoHint3": "Contacts will be able to see this information in Conversation Settings ",
    "profileInfoHint2": "You can add up to 3 fields.",
    "profileInfoHint": "Add some public information about yourself here e.g. blog, websites, brief bio.",
    "availabilityStatusBusy": "Busy",
    "availabilityStatusAway": "Away",
    "availabilityStatusAvailable": "Available",
    "blodeuweddWarning": "Blodeuwedd uses a local language model and a set of small auxiliary models to power its functionality. These techniques are often very effective they are not without error. \n\nWhile we have taken efforts to minimize the risk, there is still the possibility that Blodeuwedd outputs will be incorrect, hallucinated and\/or offensive.\n\nBecause of that Blodeuwedd requires downloading two additional components separate from Cwtch, the Blodeuwedd Model (or a compatible model) and the Blodeuwedd Runner. \n\nSee https:\/\/docs.cwtch.im\/docs\/settings\/experiments\/blodeuwedd for more information on obtaining these components and setting them up.",
    "blodeuweddProcessing": "Blodeuwedd is processing...",
    "blodeuweddTranslate": "Translate Message",
    "blodeuweddSummarize": "Summarize Conversation",
    "blodeuweddPath": "The directory where the Blodeuwedd is located on your computer.",
    "blodeuweddNotSupported": "This version of Cwtch has been compiled without support for the Blodeuwedd Assistant.",
    "blodeuweddDescription": "The Blodeuwedd assistant adds new features to Cwtch such as chat transcript summarization and message translation via a locally hosted language model.",
    "blodeuweddExperimentEnable": "Blodeuwedd Assistant",
    "localeKo": "Korean \/ 한국어",
    "localeSk": "Slovak \/ Slovák",
    "profileAutostartDescription": "Controls if the profile will be automatically launched on startup",
    "profileEnabled": "Enable",
    "profileAutostartLabel": "Autostart",
    "localePtBr": "Brazilian Portuguese \/ Português do Brasil",
    "localeNl": "Dutch \/ Dutch",
    "experimentQRCodeDescription": "QR Code support allows sharing data (such as profile identity) by QR Codes",
    "enableExperimentQRCode": "QR Codes",
    "shareMenuQRCode": "Show QR Code",
    "shareProfileMenuTooltop": "Share profile via...",
    "acquiredTicketsFromServer": "Antispam Challenge Complete",
    "acquiringTicketsFromServer": "Performing Antispam Challenge",
    "errorDownloadDirectoryDoesNotExist": "Filesharing cannot be enabled because the Download Folder has not been set, or is set to a folder that does not exist.",
    "localeTr": "Turcă \/ Türk",
    "localeIt": "Italiană",
    "localeEn": "Engleză  \/ English",
    "localeFr": "Franceză \/ Français",
    "tooltipUnpinConversation": "Unpin conversation from the top of \"Conversations\"",
    "tooltipPinConversation": "Pin conversation to the top of \"Conversations\"",
    "replyingTo": "Replying to %1",
    "fileDownloadUnavailable": "This file appears unavailable for download. The sender may have disabled downloads for this file.",
    "messageNoReplies": "There are no replies to this message.",
    "headingReplies": "Replies",
    "viewReplies": "View replies to this message",
    "restartFileShare": "Start Sharing File",
    "stopSharingFile": "Stop Sharing File",
    "manageSharedFiles": "Manage Shared Files",
    "localeDe": "Germană",
    "localeLb": "Luxemburgheză",
    "localeNo": "Norvegiană",
    "localeEl": "Greacă",
    "localePl": "Poloneză",
    "localeRo": "Română",
    "localeRU": "Rusă",
    "localeEs": "Spaniolă",
    "localeDa": "Daneză",
    "localePt": "Portugheză",
    "localeCy": "Velşă",
    "settingImagePreviewsDescription": "Imaginile vor fi descărcate și previzualizate automat. Vă rugăm să rețineți că previzualizările imaginilor pot duce adesea la vulnerabilități de securitate și nu ar trebui să activați acest Experiment dacă utilizați Cwtch cu contacte care nu sunt de încredere. Imaginile de profil sunt planificate pentru Cwtch 1.6.",
    "tooltipPreviewFormatting": "Preview Message Formatting",
    "tooltipCode": "Code \/ Monospace",
    "tooltipStrikethrough": "Strikethrough",
    "tooltipSubscript": "Subscript",
    "tooltipSuperscript": "Superscript",
    "tooltipItalicize": "Italic",
    "tooltipBackToMessageEditing": "Back to Message Editing",
    "tooltipBoldText": "Bold",
    "okButton": "OK",
    "settingsAndroidPowerReenablePopup": "Cannot re-enable Battery Optimization from within Cwtch. Please go to Android \/ Settings \/ Apps \/ Cwtch \/ Battery and set Usage to 'Optimized'",
    "settingAndroidPowerExemptionDescription": "Optional: Request Android to exempt Cwtch from optimized power management. This will result in better stability at the cost of greater battery use.",
    "settingAndroidPowerExemption": "Android Ignore Battery Optimizations",
    "thisFeatureRequiresGroupExpermientsToBeEnabled": "This feature requires the Groups Experiment to be enabled in Settings",
    "messageFormattingDescription": "Enable rich text formatting in displayed messages e.g. **bold** and *italic*",
    "formattingExperiment": "Message Formatting",
    "clickableLinkError": "Error encountered while attempting to open URL",
    "clickableLinksCopy": "Copy URL",
    "clickableLinkOpen": "Open URL",
    "clickableLinksWarning": "Opening this URL will launch an application outside of Cwtch and may reveal metadata or otherwise compromise the security of Cwtch. Only open URLs from people you trust. Are you sure you want to continue?",
    "shuttingDownApp": "Shutting down...",
    "successfullyImportedProfile": "Successfully Imported Profile: %profile",
    "failedToImportProfile": "Error Importing Profile",
    "importProfileTooltip": "Use an encrypted Cwtch backup to bring in a profile created in another instance of Cwtch.",
    "importProfile": "Import Profile",
    "exportProfileTooltip": "Backup this profile to an encrypted file. The encrypted file can be imported into another Cwtch app.",
    "exportProfile": "Export Profile",
    "deleteProfileConfirmBtn": "Sigur ștergeti profilul",
    "deleteConfirmLabel": "Tastați ȘTERGE pentru a confirma",
    "conversationNotificationPolicySettingDescription": "Controlați comportamentul de notificare al acestei conversații",
    "createGroupTitle": "Creați un grup",
    "serverLabel": "Server",
    "defaultGroupName": "Grup minunat",
    "createGroupBtn": "Creați",
    "profileOnionLabel": "Trimiteți această adresă persoanelor cu care doriți să intrați în legătură",
    "addPeerTab": "Adăugați un contact",
    "createGroupTab": "Creați un grup",
    "joinGroupTab": "Alăturați-vă unui grup",
    "peerAddress": "Adresa",
    "peerName": "Nume",
    "server": "Server",
    "invitation": "Invitație",
    "groupAddr": "Adresa",
    "addPeer": "Adăugați un contact",
    "createGroup": "Creați un grup",
    "joinGroup": "Alăturați-vă grupului",
    "newBulletinLabel": "Buletin nou",
    "postNewBulletinLabel": "Postați un buletin nou",
    "titlePlaceholder": "titlu...",
    "pasteAddressToAddContact": "Inserați aici o adresă, o invitație sau un pachet de chei pentru a adăuga o conversație nouă",
    "blocked": "Blocat",
    "search": "Căutare...",
    "invitationLabel": "Invitație",
    "serverInfo": "Informații despre server",
    "serverConnectivityConnected": "Server conectat",
    "serverConnectivityDisconnected": "Server deconectat",
    "serverSynced": "Sincronizat",
    "serverNotSynced": "Se sincronizează mesaje noi (poate dura ceva timp)...",
    "viewServerInfo": "Informații despre server",
    "groupNameLabel": "Numele grupului",
    "saveBtn": "Salvați",
    "inviteToGroupLabel": "Invitați în grup",
    "inviteBtn": "Invitați",
    "update": "Actualizați",
    "deleteBtn": "Ștergeți",
    "searchList": "Lista de căutare",
    "peerNotOnline": "Contactul este offline. Aplicațiile nu pot fi utilizate în acest moment.",
    "addListItemBtn": "Adăugați un element",
    "membershipDescription": "Mai jos este o listă a utilizatorilor care au trimis mesaje către grup. Este posibil ca această listă să nu afișeze toți utilizatorii care au acces la grup.",
    "dmTooltip": "Apăsati pentru mesaj direct",
    "couldNotSendMsgError": "Nu s-a putut trimite acest mesaj",
    "acknowledgedLabel": "Recunoscut",
    "pendingLabel": "În așteptare",
    "peerBlockedMessage": "Contactul este blocat",
    "peerOfflineMessage": "Contactul este offline, mesajele nu pot fi transmise în acest moment",
    "copyBtn": "Copiați",
    "newGroupBtn": "Creați un grup nou",
    "acceptGroupInviteLabel": "Doriți să acceptați invitația în",
    "acceptGroupBtn": "Acceptați",
    "rejectGroupBtn": "Respingeți",
    "chatBtn": "Conversație",
    "listsBtn": "Liste",
    "bulletinsBtn": "Buletine",
    "puzzleGameBtn": "Puzzle",
    "addressLabel": "Adresa",
    "copiedToClipboardNotification": "Copiat în clipboard",
    "displayNameLabel": "Numele de afișare",
    "blockBtn": "Blocați contactul",
    "savePeerHistory": "Salvați istoricul",
    "savePeerHistoryDescription": "Determină dacă se șterge istoricul asociat cu persoana de contact.",
    "dontSavePeerHistory": "Ștergeți istoricul",
    "unblockBtn": "Deblocați contactul",
    "addProfileTitle": "Adăugați un profil nou",
    "editProfileTitle": "Editați profilul",
    "profileName": "Numele afișat",
    "defaultProfileName": "Alice",
    "newProfile": "Profil nou",
    "editProfile": "Editați profilul",
    "radioUsePassword": "Parolă",
    "radioNoPassword": "Necriptat (fără parolă)",
    "noPasswordWarning": "Lipsa unei parole pe acest cont înseamnă că toate datele stocate local nu vor fi criptate.",
    "yourDisplayName": "Numele dvs. de afișare",
    "currentPasswordLabel": "Parola actuală",
    "password1Label": "Parolă",
    "password2Label": "Reintroduceți parola",
    "passwordErrorEmpty": "Parola nu poate fi goală",
    "createProfileBtn": "Creați un profil",
    "saveProfileBtn": "Salvați profilul",
    "passwordErrorMatch": "Parolele nu se potrivesc",
    "passwordChangeError": "Eroare la schimbarea parolei: Parola furnizată a fost respinsă",
    "deleteProfileBtn": "Ștergeți profilul",
    "deleteConfirmText": "ȘTERGE",
    "addNewProfileBtn": "Adăugați un profil nou",
    "enterProfilePassword": "Introduceți o parolă pentru a vă vizualiza profilurile",
    "password": "Parolă",
    "error0ProfilesLoadedForPassword": "0 profiluri încărcate cu această parolă",
    "yourProfiles": "Profilurile dvs.",
    "yourServers": "Serverele dvs.",
    "unlock": "Deblocați",
    "cwtchSettingsTitle": "Setări Cwtch",
    "builddate": "Construit pe: %2",
    "versionBuilddate": "Versiune: %1 Construit pe: %2",
    "zoomLabel": "Zoomul interfeței (afectează dimensiunile textului și ale butoanelor)",
    "blockUnknownLabel": "Blocați contactele necunoscute",
    "settingLanguage": "Limba",
    "settingInterfaceZoom": "Nivelul de zoom",
    "largeTextLabel": "Mare",
    "settingTheme": "Utilizați teme luminoase",
    "themeLight": "Luminos",
    "themeDark": "Întunecat",
    "experimentsEnabled": "Activați experimentele",
    "versionTor": "Versiunea %1 cu tor %2",
    "version": "Versiunea %1",
    "smallTextLabel": "Mic",
    "loadingTor": "Se încarcă tor...",
    "viewGroupMembershipTooltip": "Vizualizați membrii grupului",
    "networkStatusDisconnected": "Aplicația a fost deconectată de la internet, verificați conexiunea",
    "networkStatusAttemptingTor": "Se incearcă conectarea la rețeaua Tor",
    "networkStatusConnecting": "Se conectează la rețea și contacte...",
    "networkStatusOnline": "Online",
    "newConnectionPaneTitle": "Conexiune nouă",
    "addListItem": "Adăugați o nouă listă",
    "addNewItem": "Adăugați un nou element în listă",
    "todoPlaceholder": "În construcție...",
    "enableGroups": "Activează chatul de grup",
    "enterCurrentPasswordForDelete": "Vă rugăm să introduceți parola actuală pentru a șterge acest profil.",
    "conversationSettings": "Setările conversației",
    "invalidImportString": "Șir de caractere de import nevalabil",
    "contactAlreadyExists": "Contactul există deja",
    "tooltipOpenSettings": "Deschideți panoul de setări",
    "tooltipAddContact": "Adăugați un contact sau o conversație nouă",
    "titleManageContacts": "Conversații",
    "tooltipUnlockProfiles": "Deblocați profilurile criptate introducând parola acestora.",
    "titleManageProfiles": "Gestionați profilurile Cwtch",
    "descriptionExperiments": "Experimentele Cwtch sunt opționale, caracteristici opt-in care adaugă funcționalități suplimentare la Cwtch care pot avea considerații diferite privind confidențialitatea față de chat-ul tradițional rezistent la metadate 1:1, de exemplu chat-ul de grup, integrarea boților etc.",
    "descriptionExperimentsGroups": "Experimentul de grup îi permite lui Cwtch să se conecteze la o infrastructură de server nesigură pentru a facilita comunicarea cu mai multe persoane de contact.",
    "descriptionBlockUnknownConnections": "Dacă este activată, această opțiune va închide automat conexiunile de la utilizatorii Cwtch care nu au fost adăugați la lista dvs. de contacte.",
    "successfullAddedContact": "Adăugat cu succes ",
    "titleManageServers": "Gestionați serverele",
    "inviteToGroup": "Ați fost invitat să vă alăturați unui grup:",
    "leaveConversation": "Părăsiți această conversație",
    "reallyLeaveThisGroupPrompt": "Sigur vrei să părăsești această conversație? Toate mesajele și atributele vor fi șterse.",
    "yesLeave": "Da, părăsește această conversație",
    "newPassword": "Parolă nouă",
    "chatHistoryDefault": "Această conversație va fi ștearsă când aplicația Cwtch va fi închisă! Istoricul mesajelor poate fi activat pentru fiecare conversație în parte prin intermediul meniului Setări din dreapta sus.",
    "accepted": "Admis!",
    "rejected": "Respins!",
    "contactSuggestion": "Aceasta este o sugestie de contact pentru: ",
    "sendAnInvitation": "Ați trimis o invitație pentru: ",
    "torStatus": "Starea Tor",
    "torVersion": "Versiunea Tor",
    "resetTor": "Resetare",
    "cancel": "Anulare",
    "sendMessage": "Trimiteți un mesaj",
    "sendInvite": "Trimiteți o invitație de contact sau de grup",
    "deleteProfileSuccess": "Profil a fost șters cu succes",
    "addServerFirst": "Trebuie să adăugați un server înainte de a putea crea un grup",
    "nickChangeSuccess": "Porecla profilului a fost schimbată cu succes",
    "createProfileToBegin": "Creați sau deblocați un profil pentru a începe",
    "addContactFirst": "Adăugați sau alegeți un contact pentru a începe să discutați.",
    "torNetworkStatus": "Starea rețelei Tor",
    "debugLog": "Activați jurnalizarea de depanare a consolei",
    "profileDeleteSuccess": "Profil a fost șters cu succes",
    "malformedMessage": "Mesaj incorect",
    "shutdownCwtchTooltip": "Oprire Cwtch",
    "shutdownCwtchDialogTitle": "Oprire Cwtch?",
    "shutdownCwtchDialog": "Sigur doriți să opriți Cwtch? Se vor închide toate conexiunile și se va închide aplicația.",
    "shutdownCwtchAction": "Opriți Cwtch",
    "groupInviteSettingsWarning": "Ați fost invitat să vă alăturați unui grup! Vă rugăm să activați Experimentul de chat în grup din Setări pentru a vedea această invitație.",
    "tooltipShowPassword": "Afișați parola",
    "tooltipHidePassword": "Ascundeți parola",
    "notificationNewMessageFromPeer": "Mesaj nou de la un contact!",
    "notificationNewMessageFromGroup": "Mesaj nou într-un grup!",
    "tooltipAcceptContactRequest": "Acceptați această cerere de contact.",
    "tooltipRejectContactRequest": "Respingeți această cerere de contact",
    "tooltipReplyToThisMessage": "Răspundeți la acest mesaj",
    "tooltipRemoveThisQuotedMessage": "Eliminați mesajul citat.",
    "settingUIColumnPortrait": "Coloane UI în modul Portret",
    "settingUIColumnLandscape": "Coloane UI în modul Peisaj",
    "settingUIColumnSingle": "Singur",
    "settingUIColumnDouble12Ratio": "Dublu (1:2)",
    "settingUIColumnDouble14Ratio": "Dublu (1:4)",
    "settingUIColumnOptionSame": "La fel ca modul portret",
    "contactGoto": "Accesați conversația cu %1",
    "addContact": "Adăugați un contact",
    "addContactConfirm": "Adăugați contactul %1",
    "encryptedServerDescription": "Criptarea unui server cu o parolă îl protejează de alte persoane care utilizează acest dispozitiv. Serverele criptate nu pot fi decriptate, afișate sau accesate până când nu se introduce parola corectă pentru a le debloca.",
    "encryptedProfileDescription": "Criptarea unui profil cu o parolă îl protejează de alte persoane care utilizează și ele acest dispozitiv. Profilurile criptate nu pot fi decriptate, afișate sau accesate până când nu se introduce parola corectă pentru a le debloca.",
    "plainProfileDescription": "Vă recomandăm să vă protejați profilurile Cwtch cu o parolă. Dacă nu setați o parolă pe acest profil, atunci oricine are acces la acest dispozitiv are acces la informații despre acest profil, inclusiv la chei criptografice importante.",
    "placeholderEnterMessage": "Introduceți un mesaj...",
    "blockedMessageMessage": "Acest mesaj provine de la un profil pe care l-ați blocat.",
    "showMessageButton": "Afișați mesajul",
    "blockUnknownConnectionsEnabledDescription": "Conexiunile de la contacte necunoscute sunt blocate. Puteți modifica acest lucru în Setări",
    "archiveConversation": "Arhivați această conversație",
    "streamerModeLabel": "Modul de Streamer\/Prezentare",
    "descriptionStreamerMode": "Dacă este activată, această opțiune face ca aplicația să fie mai privată din punct de vedere vizual pentru streaming sau prezentare. De exemplu, ascunde adresele de profil și de contact",
    "retrievingManifestMessage": "Se preiau informații despre fișier...",
    "openFolderButton": "Deschideți dosarul",
    "downloadFileButton": "Descărcați",
    "labelFilename": "Numele fișierului",
    "labelFilesize": "Dimensiune",
    "messageEnableFileSharing": "Activați experimentul de partajare a fișierelor pentru a vizualiza acest mesaj.",
    "messageFileSent": "Ați trimis un fișier",
    "messageFileOffered": "Contactul vrea să vă trimită un fișier",
    "tooltipSendFile": "Trimiteți fișierul",
    "settingFileSharing": "Partajarea fișierelor",
    "descriptionFileSharing": "Experimentul de partajare a fișierelor vă permite să trimiteți și să primiți fișiere de la contacte și grupuri Cwtch. Rețineți că partajarea unui fișier cu un grup va duce la conectarea membrilor acelui grup cu dvs. direct prin Cwtch pentru a-l descărca.",
    "titleManageProfilesShort": "Profiluri",
    "addServerTitle": "Adăugați un server",
    "editServerTitle": "Editați serverul",
    "serverAddress": "Adresa serverului",
    "serverDescriptionLabel": "Descrierea serverului",
    "serverDescriptionDescription": "Descrierea dvs. a serverului este doar pentru uz personal de gestionare și nu va fi niciodată împărtășită.",
    "serverEnabled": "Server pornit",
    "serverEnabledDescription": "Porniți sau opriți serverul",
    "serverAutostartLabel": "Pornire automată",
    "serverAutostartDescription": "Alege dacă aplicația va lansa automat serverul la pornire",
    "saveServerButton": "Salvați serverul",
    "serversManagerTitleLong": "Serverele pe care le găzduiți",
    "serversManagerTitleShort": "Servere",
    "addServerTooltip": "Adăugați un server nou",
    "unlockServerTip": "Creați sau accesați un server pentru a începe!",
    "unlockProfileTip": "Creați sau accesați un profil pentru a începe!",
    "enterServerPassword": "Introduceți parola pentru a debloca serverul",
    "settingServers": "Găzduirea serverelor",
    "settingServersDescription": "Experimentul serverelor de găzduire permite găzduirea și gestionarea serverelor Cwtch",
    "copyAddress": "Copiați adresa",
    "enterCurrentPasswordForDeleteServer": "Vă rugăm să introduceți parola actuală pentru a șterge acest server",
    "deleteServerSuccess": "Serverul a fost șters cu succes",
    "plainServerDescription": "Vă recomandăm să vă protejați serverele Cwtch cu o parolă. Dacă nu setați o parolă pe acest server, atunci oricine are acces la acest dispozitiva are acces la informații despre acest server, inclusiv la chei criptografice importante.",
    "fileSavedTo": "Salvat în",
    "fileInterrupted": "Întrerupt",
    "fileCheckingStatus": "Se verifică starea descărcării",
    "verfiyResumeButton": "Verificați\/reluați",
    "copyServerKeys": "Copiați cheile",
    "newMessagesLabel": "Mesaje noi",
    "importLocalServerLabel": "Importați un server găzduit local",
    "importLocalServerSelectText": "Selectați Server local",
    "importLocalServerButton": "Importă %1",
    "fieldDescriptionLabel": "Descriere",
    "manageKnownServersButton": "Gestionați serverele cunoscute",
    "displayNameTooltip": "Vă rugăm să introduceți un nume de afișat",
    "manageKnownServersLong": "Gestionați serverele cunoscute",
    "manageKnownServersShort": "Servere",
    "serverMetricsLabel": "Datele serverului",
    "serverTotalMessagesLabel": "Total mesaje",
    "serverConnectionsLabel": "Conexiune",
    "enableExperimentClickableLinks": "Activați linkurile pe care se poate da clic",
    "experimentClickableLinksDescription": "Experimentul cu linkuri pe care se poate da clic vă permite să faceți clic pe adresele URL partajate în mesaje",
    "settingImagePreviews": "Previzualizări de imagini și poze de profil",
    "settingDownloadFolder": "Fișier de descărcări",
    "themeNameCwtch": "Cwtch",
    "themeNameWitch": "Vrăjitoare",
    "themeNameVampire": "Vampir",
    "themeNameGhost": "Fantomă",
    "themeNamePumpkin": "Dovleac",
    "themeNameMermaid": "Sirenă",
    "themeNameMidnight": "Miezul nopţii",
    "themeNameNeon1": "Neon1",
    "themeNameNeon2": "Neon2",
    "themeColorLabel": "Culori",
    "loadingCwtch": "Se încarcă Cwtch...",
    "storageMigrationModalMessage": "Migrarea profilurilor către un nou format de stocare. Va dura câteva minute...",
    "msgFileTooBig": "Dimensiunea fișierului nu poate depăși 10 GB",
    "msgConfirmSend": "Sunteți sigur că doriți să trimiteți",
    "btnSendFile": "Trimiteți fișierul",
    "msgAddToAccept": "Adăugați acest cont la contactele dvs. pentru a accepta acest fișier.",
    "torSettingsEnabledAdvanced": "Activați configurarea avansată pentru Tor",
    "torSettingsEnabledAdvancedDescription": "Utilizați un serviciu Tor existent pe sistemul dumneavoastră sau modificați parametrii serviciului Tor Cwtch.",
    "torSettingsCustomSocksPort": "Port SOCKS personalizat",
    "torSettingsCustomSocksPortDescription": "Utilizați un port personalizat pentru conexiunile de date la proxy-ul Tor",
    "torSettingsCustomControlPort": "Port de control personalizat",
    "torSettingsCustomControlPortDescription": "Utilizați un port personalizat pentru controlul conexiunilor la proxy-ul Tor",
    "torSettingsUseCustomTorServiceConfiguration": "Utilizați o configurație personalizată a serviciului Tor (torrc)",
    "torSettingsUseCustomTorServiceConfigurastionDescription": "Ignorați configurația implicită tor. Avertisment: Acest lucru ar putea fi periculos. Activați acest lucru doar dacă știți ce faceți.",
    "torSettingsErrorSettingPort": "Numărul de port trebuie să fie între 1 și 65535",
    "fileSharingSettingsDownloadFolderDescription": "Când fișierele sunt descărcate automat (de exemplu, imaginile, când sunt activate previzualizările de imagine), este necesară o locație implicită în care să se descarce fișierele.",
    "fileSharingSettingsDownloadFolderTooltip": "Răsfoiți pentru a selecta un alt dosar implicit pentru fișierele descărcate.",
    "labelACNCircuitInfo": "Informații despre circuitul ACN",
    "descriptionACNCircuitInfo": "Informații detaliate despre metoda pe care rețeaua de comunicare anonimă o folosește pentru a se conecta la această conversație.",
    "labelTorNetwork": "Rețeaua Tor",
    "torSettingsEnableCache": "Stocheaza consensul Tor in memoria cache",
    "torSettingsEnabledCacheDescription": "Stochează în memoria cache consensul Tor descărcat pentru a fi reutilizat data viitoare când se deschide Cwtch. Acest lucru va permite ca Tor să pornească mai repede. Când este dezactivat, Cwtch va curăța datele din memoria cache la pornire.",
    "tooltipSelectACustomProfileImage": "Selectați o imagine de profil personalizată",
    "notificationPolicyMute": "Mut",
    "notificationPolicyOptIn": "Permite",
    "notificationPolicyDefaultAll": "Toate implicite",
    "conversationNotificationPolicyDefault": "Implicit",
    "conversationNotificationPolicyOptIn": "Permite",
    "conversationNotificationPolicyNever": "Niciodată",
    "notificationPolicySettingLabel": "Politica de notificare",
    "notificationContentSettingLabel": "Conținutul notificărilor",
    "notificationPolicySettingDescription": "Controlează comportamentul implicit de notificare a aplicației",
    "notificationContentSettingDescription": "Controlează conținutul notificărilor din conversație",
    "settingGroupBehaviour": "Comportament",
    "settingsGroupAppearance": "Aspect",
    "settingsGroupExperiments": "Experimente",
    "conversationNotificationPolicySettingLabel": "Politica de notificare a conversațiilor",
    "notificationContentSimpleEvent": "Eveniment simplu",
    "notificationContentContactInfo": "Informații despre conversație",
    "newMessageNotificationSimple": "Mesaj nou",
    "newMessageNotificationConversationInfo": "Mesaj nou de la %1"
}