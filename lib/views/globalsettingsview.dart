import 'dart:convert';
import 'package:cwtch/cwtch_icons_icons.dart';
import 'package:cwtch/views/globalsettingsaboutview.dart';
import 'package:cwtch/views/globalsettingsappearanceview.dart';
import 'package:cwtch/views/globalsettingsbehaviourview.dart';
import 'package:cwtch/views/globalsettingsexperimentsview.dart';
import 'package:flutter/material.dart';
import 'package:cwtch/settings.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../main.dart';

/// Global Settings View provides access to modify all the Globally Relevant Settings including Locale, Theme and Experiments.
class GlobalSettingsView extends StatefulWidget {
  @override
  _GlobalSettingsViewState createState() => _GlobalSettingsViewState();
}

class _GlobalSettingsViewState extends State<GlobalSettingsView> {
  ScrollController settingsListScrollController = ScrollController();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
              title: Text(AppLocalizations.of(context)!.cwtchSettingsTitle),
              bottom: TabBar(
                isScrollable: true,
                tabs: [
                  Tab(key: Key("OpenSettingsAppearance"), icon: Icon(Icons.palette), text: AppLocalizations.of(context)!.settingsGroupAppearance),
                  Tab(key: Key("OpenSettingsBehaviour"), icon: Icon(Icons.settings), text: AppLocalizations.of(context)!.settingGroupBehaviour),
                  Tab(key: Key("OpenSettingsExperiments"), icon: Icon(CwtchIcons.enable_experiments), text: AppLocalizations.of(context)!.settingsGroupExperiments),
                  Tab(icon: Icon(Icons.info), text: AppLocalizations.of(context)!.settingsGroupAbout),
                ],
              )),
          body: _buildSettingsList(),
        ));
  }

  Widget _buildSettingsList() {
    return Consumer<Settings>(builder: (ccontext, settings, child) {
      return LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return TabBarView(children: [
          GlobalSettingsAppearanceView(),
          GlobalSettingsBehaviourView(),
          GlobalSettingsExperimentsView(),
          GlobalSettingsAboutView(),
        ]);
      });
    });
  }
}

/// Send an UpdateGlobalSettings to the Event Bus
saveSettings(context) {
  var settings = Provider.of<Settings>(context, listen: false);
  Provider.of<FlwtchState>(context, listen: false).cwtch.UpdateSettings(jsonEncode(settings.asJson()));
}
