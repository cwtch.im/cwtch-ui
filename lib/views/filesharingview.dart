import 'dart:convert';

import 'package:cwtch/main.dart';
import 'package:cwtch/models/contact.dart';
import 'package:cwtch/models/profile.dart';
import 'package:cwtch/settings.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

import '../cwtch_icons_icons.dart';

class FileSharingView extends StatefulWidget {
  @override
  _FileSharingViewState createState() => _FileSharingViewState();
}

class _FileSharingViewState extends State<FileSharingView> {
  @override
  Widget build(BuildContext context) {
    var handle = Provider.of<ContactInfoState>(context).nickname;
    if (handle.isEmpty) {
      handle = Provider.of<ContactInfoState>(context).onion;
    }

    var profileHandle = Provider.of<ProfileInfoState>(context).onion;

    return Scaffold(
      appBar: AppBar(
        title: Text(handle + " » " + AppLocalizations.of(context)!.manageSharedFiles),
      ),
      backgroundColor: Provider.of<Settings>(context).theme.backgroundMainColor,
      body: FutureBuilder(
        future: Provider.of<FlwtchState>(context, listen: false).cwtch.GetSharedFiles(profileHandle, Provider.of<ContactInfoState>(context).identifier),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<dynamic> sharedFiles = jsonDecode(snapshot.data as String) ?? List<dynamic>.empty();
            // Stabilize sort when files are assigned identical DateShared.
            sharedFiles.sort((a, b) {
              final cmp = a["DateShared"].toString().compareTo(b["DateShared"].toString());
              return cmp == 0 ? a["FileKey"].compareTo(b["FileKey"]) : cmp;
            });
            sharedFiles = injectTimeHeaders(AppLocalizations.of(context), sharedFiles, DateTime.now());

            var fileList = ScrollablePositionedList.separated(
              itemScrollController: ItemScrollController(),
              itemCount: sharedFiles.length,
              shrinkWrap: true,
              reverse: true,
              physics: BouncingScrollPhysics(),
              semanticChildCount: sharedFiles.length,
              itemBuilder: (context, index) {
                // New for #589: some entries in the time-sorted list
                // are placeholders for relative time headers. These should
                // be placed in simple tiles for aesthetic pleasure.
                // The dict only contains TimeHeader key, and as such
                // the downstream logic needs to be avoided.
                if (sharedFiles[index].containsKey("TimeHeader")) {
                  return ListTile(title: Text(sharedFiles[index]["TimeHeader"]), dense: true);
                }
                String filekey = sharedFiles[index]["FileKey"];
                // This makes the UI *very* slow when enabled. But can be useful for debugging
                // Uncomment if necessary.
                // EnvironmentConfig.debugLog("$sharedFiles " + sharedFiles[index].toString());
                return SwitchListTile(
                    title: Text(sharedFiles[index]["Path"]),
                    subtitle: Text(sharedFiles[index]["DateShared"]),
                    value: sharedFiles[index]["Active"],
                    activeTrackColor: Provider.of<Settings>(context).theme.defaultButtonColor,
                    inactiveTrackColor: Provider.of<Settings>(context).theme.defaultButtonDisabledColor,
                    secondary: Icon(CwtchIcons.attached_file_3, color: Provider.of<Settings>(context).current().mainTextColor),
                    onChanged: (newValue) {
                      setState(() {
                        if (newValue) {
                          Provider.of<FlwtchState>(context, listen: false).cwtch.RestartSharing(profileHandle, filekey);
                        } else {
                          Provider.of<FlwtchState>(context, listen: false).cwtch.StopSharing(profileHandle, filekey);
                        }
                      });
                    });
              },
              // Here is seemingly approximately the location where date dividers could be injected.
              // See #589 fore description. Seems like this lambda could get refactored out into
              // a testable function that determines time in the past relative to now and emits
              // a localized text banner as necessary. Time and language localization creates
              // a possible difficulty for this one.
              separatorBuilder: (BuildContext context, int index) {
                return Divider(height: 1);
              },
            );
            return fileList;
          }
          return Container();
        },
      ),
    );
  }
}

bool isToday(DateTime inputTimestamp, DateTime inputNow) {
  final localTimestamp = inputTimestamp.toLocal();
  final localNow = inputNow.toLocal();
  return localTimestamp.day == localNow.day && localTimestamp.month == localNow.month && localTimestamp.year == localNow.year;
}

bool isYesterday(DateTime inputTimestamp, DateTime inputNow) {
  final adjustedTimestamp = inputTimestamp.add(const Duration(hours: 24));
  return isToday(adjustedTimestamp, inputNow);
}

bool isThisWeek(DateTime inputTimestamp, DateTime inputNow) {
  // note that dart internally measures weeks starting from Mondays,
  // so the idea of something being earlier this week is finicky.
  final localTimestamp = inputTimestamp.toLocal();
  final localNow = inputNow.toLocal();
  final thisWeekLb = DateTime(localNow.year, localNow.month, localNow.day).subtract(Duration(days: localNow.weekday - 1, microseconds: 1));
  final thisWeekUb = thisWeekLb.add(const Duration(days: DateTime.daysPerWeek, microseconds: 1));
  return thisWeekLb.isBefore(localTimestamp) && thisWeekUb.isAfter(localTimestamp);
}

bool isLastWeek(DateTime inputTimestamp, DateTime inputNow) {
  // this inherits the same week definition issues as isThisWeek.
  final adjustedTimestamp = inputTimestamp.toLocal().add(Duration(days: DateTime.daysPerWeek));
  return isThisWeek(adjustedTimestamp, inputNow);
}

bool isThisMonth(DateTime inputTimestamp, DateTime inputNow) {
  final localTimestamp = inputTimestamp.toLocal();
  final localNow = inputNow.toLocal();
  return localTimestamp.month == localNow.month && localTimestamp.year == localNow.year;
}

bool isLastMonth(DateTime inputTimestamp, DateTime inputNow) {
  final localTimestamp = inputTimestamp.toLocal();
  final localNow = inputNow.toLocal();
  return (localTimestamp.year == localNow.year && localTimestamp.month == localNow.month - 1) ||
      (localTimestamp.year == localNow.year - 1 && localTimestamp.month == DateTime.december && localNow.month == DateTime.january);
}

bool isThisYear(DateTime inputTimestamp, DateTime inputNow) {
  final localTimestamp = inputTimestamp.toLocal();
  final localNow = inputNow.toLocal();
  return localTimestamp.year == localNow.year;
}

bool isEarlierThanThisYear(DateTime inputTimestamp, DateTime inputNow) {
  final localTimestamp = inputTimestamp.toLocal();
  final localNow = inputNow.toLocal();
  return localTimestamp.year < localNow.year;
}

enum FileRelativeTime {
  Today,
  Yesterday,
  ThisWeek,
  LastWeek,
  ThisMonth,
  LastMonth,
  ThisYear,
  Earlier,
}

FileRelativeTime assignRelativeTime(DateTime timestamp, DateTime now) {
  if (isToday(timestamp, now)) {
    return FileRelativeTime.Today;
  } else if (isYesterday(timestamp, now)) {
    return FileRelativeTime.Yesterday;
  } else if (isThisWeek(timestamp, now)) {
    return FileRelativeTime.ThisWeek;
  } else if (isLastWeek(timestamp, now)) {
    return FileRelativeTime.LastWeek;
  } else if (isThisMonth(timestamp, now)) {
    return FileRelativeTime.ThisMonth;
  } else if (isLastMonth(timestamp, now)) {
    return FileRelativeTime.LastMonth;
  } else if (isThisYear(timestamp, now)) {
    return FileRelativeTime.ThisYear;
  } else if (isEarlierThanThisYear(timestamp, now)) {
    return FileRelativeTime.Earlier;
  } else {
    throw Exception("assignRelativeTime: impossible time comparison encountered, likely bug in cwtch-ui");
  }
}

String getLocalizedRelativeTime(dynamic localizer, FileRelativeTime t) {
  switch (t) {
    case FileRelativeTime.Today:
      return localizer!.relativeTimeToday;
    case FileRelativeTime.Yesterday:
      return localizer!.relativeTimeYesterday;
    case FileRelativeTime.ThisWeek:
      return localizer!.relativeTimeThisWeek;
    case FileRelativeTime.LastWeek:
      return localizer!.relativeTimeLastWeek;
    case FileRelativeTime.ThisMonth:
      return localizer!.relativeTimeThisMonth;
    case FileRelativeTime.LastMonth:
      return localizer!.relativeTimeLastMonth;
    case FileRelativeTime.ThisYear:
      return localizer!.relativeTimeThisYear;
    case FileRelativeTime.Earlier:
      return localizer!.relativeTimeEarlier;
    default:
      return "erroneous unlocalized time descriptor";
  }
}

List<dynamic> injectTimeHeaders(dynamic localizer, List<dynamic> sharedFiles, DateTime currentTime) {
  final result = <dynamic>[];
  var previousFileTime = FileRelativeTime.Today;
  for (var i = 0; i < sharedFiles.length; ++i) {
    final fileTime = DateTime.parse(sharedFiles[i]["DateShared"]);
    final relativeTime = assignRelativeTime(fileTime, currentTime);
    if (i == 0) {
      previousFileTime = relativeTime;
    }
    if (previousFileTime != relativeTime) {
      result.insert(result.length, {"TimeHeader": getLocalizedRelativeTime(localizer, previousFileTime)});
      previousFileTime = relativeTime;
    }
    result.insert(result.length, sharedFiles[i]);
    if (i == sharedFiles.length - 1) {
      result.insert(result.length, {"TimeHeader": getLocalizedRelativeTime(localizer, relativeTime)});
    }
  }
  return result;
}
