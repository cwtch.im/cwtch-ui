import 'dart:collection';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../config.dart';
import '../cwtch_icons_icons.dart';
import '../main.dart';
import '../settings.dart';
import 'globalsettingsview.dart';

class GlobalSettingsAboutView extends StatefulWidget {
  @override
  _GlobalSettingsAboutViewState createState() => _GlobalSettingsAboutViewState();
}

class _GlobalSettingsAboutViewState extends State<GlobalSettingsAboutView> {
  ScrollController settingsListScrollController = ScrollController();

  Widget build(BuildContext context) {
    return Consumer<Settings>(builder: (ccontext, settings, child) {
      return LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints) {
        var appIcon = Icon(Icons.info, color: settings.current().mainTextColor);
        return Scrollbar(
            key: Key("AboutSettingsView"),
            trackVisibility: true,
            controller: settingsListScrollController,
            child: SingleChildScrollView(
                clipBehavior: Clip.antiAlias,
                controller: settingsListScrollController,
                child: ConstrainedBox(
                    constraints: BoxConstraints(minHeight: viewportConstraints.maxHeight, maxWidth: viewportConstraints.maxWidth),
                    child: Container(
                        color: settings.theme.backgroundPaneColor,
                        child: Column(children: [
                          AboutListTile(
                              icon: appIcon,
                              applicationIcon: Padding(padding: EdgeInsets.all(5), child: Icon(CwtchIcons.cwtch_knott)),
                              applicationName: "Cwtch UI",
                              applicationLegalese: '\u{a9} 2021-2025 Open Privacy Research Society',
                              aboutBoxChildren: <Widget>[
                                Padding(
                                  padding: EdgeInsets.fromLTRB(24.0 + 10.0 + (appIcon.size ?? 24.0), 16.0, 0.0, 0.0),
                                  // About has 24 padding (ln 389) and there appears to be another 10 of padding in the widget
                                  child: SelectableText(AppLocalizations.of(context)!.versionBuilddate.replaceAll("%1", EnvironmentConfig.BUILD_VER).replaceAll("%2", EnvironmentConfig.BUILD_DATE)),
                                )
                              ]),
                          SwitchListTile(
                            // TODO: Translate, Remove, OR Hide Prior to Release
                            title: Text(AppLocalizations.of(context)!.settingsExperimentsShowPerformanceTitle),
                            subtitle: Text(AppLocalizations.of(context)!.settingsExperimentsShowPerformanceDescription),
                            value: settings.profileMode,
                            onChanged: (bool value) {
                              setState(() {
                                if (value) {
                                  settings.profileMode = value;
                                } else {
                                  settings.profileMode = value;
                                }
                              });
                            },
                            activeTrackColor: settings.theme.defaultButtonActiveColor,
                            inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                            secondary: Icon(Icons.bar_chart, color: settings.current().mainTextColor),
                          ),
                          Visibility(
                              visible: EnvironmentConfig.BUILD_VER == dev_version && !Platform.isAndroid,
                              child: SwitchListTile(
                                title: Text("Show Semantic Debugger"),
                                subtitle: Text("Show Accessibility Debugging View"),
                                value: settings.useSemanticDebugger,
                                onChanged: (bool value) {
                                  if (value) {
                                    settings.useSemanticDebugger = value;
                                  } else {
                                    settings.useSemanticDebugger = value;
                                  }
                                  saveSettings(context);
                                },
                                activeTrackColor: settings.theme.defaultButtonActiveColor,
                                inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                                secondary: Icon(Icons.settings_accessibility, color: settings.current().mainTextColor),
                              )),
                          Visibility(
                            visible: EnvironmentConfig.BUILD_VER == dev_version && !Platform.isAndroid,
                            child: FutureBuilder(
                              future: EnvironmentConfig.BUILD_VER != dev_version || Platform.isAndroid ? null : Provider.of<FlwtchState>(context).cwtch.GetDebugInfo(),
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  return Column(
                                    children: [
                                      Text("libCwtch Debug Info: " + snapshot.data.toString()),
                                      Text("Message Cache Size (Mb): " + (Provider.of<FlwtchState>(context).profs.cacheMemUsage() / (1024 * 1024)).toString())
                                    ],
                                  );
                                } else {
                                  return Container();
                                }
                              },
                            ),
                          ),
                          Visibility(
                              visible: EnvironmentConfig.BUILD_VER == dev_version,
                              child: FutureBuilder(
                                  future: Provider.of<FlwtchState>(context).cwtch.PlatformChannelInfo(),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      HashMap<String, String> data = snapshot.data as HashMap<String, String>;
                                      return getPlatformInfo(settings, data);
                                    }
                                    return Container();
                                  }))
                        ])))));
      });
    });
  }

  getPlatformInfo(settings, HashMap<String, String> platformChannelInfo) {
    var sortedKeys = platformChannelInfo.keys.toList();
    sortedKeys.sort();
    var widgets = List<Widget>.empty(growable: true);
    sortedKeys.forEach((element) {
      widgets.add(ListTile(
        leading: Icon(Icons.android, color: settings.current().mainTextColor),
        title: Text(element),
        subtitle: Text(platformChannelInfo[element]!),
      ));
    });
    return Column(
      children: widgets,
    );
  }

  // TODO: deprecated ?
  /// Construct a version string from Package Info
  String constructVersionString(PackageInfo pinfo) {
    return pinfo.version + "." + pinfo.buildNumber;
  }
}
