import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../cwtch_icons_icons.dart';
import '../main.dart';
import '../models/servers.dart';
import '../settings.dart';
import '../themes/opaque.dart';
import '../widgets/folderpicker.dart';
import 'globalsettingsview.dart';

class GlobalSettingsExperimentsView extends StatefulWidget {
  @override
  _GlobalSettingsExperimentsViewState createState() => _GlobalSettingsExperimentsViewState();
}

class _GlobalSettingsExperimentsViewState extends State<GlobalSettingsExperimentsView> {
  ScrollController settingsListScrollController = ScrollController();

  Widget build(BuildContext context) {
    return Consumer<Settings>(builder: (ccontext, settings, child) {
      return LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return Scrollbar(
            key: Key("ExperimentsSettingsView"),
            trackVisibility: true,
            controller: settingsListScrollController,
            child: SingleChildScrollView(
                clipBehavior: Clip.antiAlias,
                controller: settingsListScrollController,
                child: ConstrainedBox(
                    constraints: BoxConstraints(minHeight: viewportConstraints.maxHeight, maxWidth: viewportConstraints.maxWidth),
                    child: Container(
                        color: settings.theme.backgroundPaneColor,
                        child: Column(children: [
                          SwitchListTile(
                            title: Text(AppLocalizations.of(context)!.experimentsEnabled),
                            subtitle: Text(AppLocalizations.of(context)!.descriptionExperiments),
                            value: settings.experimentsEnabled,
                            onChanged: (bool value) {
                              if (value) {
                                settings.enableExperiments();
                              } else {
                                settings.disableExperiments();
                              }
                              // Save Settings...
                              saveSettings(context);
                            },
                            activeTrackColor: settings.theme.defaultButtonColor,
                            inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                            secondary: Icon(CwtchIcons.enable_experiments, color: settings.current().mainTextColor),
                          ),
                          Visibility(
                              visible: settings.experimentsEnabled,
                              child: Column(
                                children: [
                                  SwitchListTile(
                                    title: Text(AppLocalizations.of(context)!.enableGroups),
                                    subtitle: Text(AppLocalizations.of(context)!.descriptionExperimentsGroups),
                                    value: settings.isExperimentEnabled(TapirGroupsExperiment),
                                    onChanged: (bool value) {
                                      if (value) {
                                        settings.enableExperiment(TapirGroupsExperiment);
                                      } else {
                                        settings.disableExperiment(TapirGroupsExperiment);
                                      }
                                      // Save Settings...
                                      saveSettings(context);
                                    },
                                    activeTrackColor: settings.theme.defaultButtonColor,
                                    inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                                    secondary: Icon(CwtchIcons.enable_groups, color: settings.current().mainTextColor),
                                  ),
                                  Visibility(
                                      visible: !Platform.isAndroid && !Platform.isIOS,
                                      child: SwitchListTile(
                                        title: Text(AppLocalizations.of(context)!.settingServers),
                                        subtitle: Provider.of<FlwtchState>(context, listen: false).cwtch.IsServersCompiled()
                                            ? Text(AppLocalizations.of(context)!.settingServersDescription)
                                            : Text("This version of Cwtch has been compiled without support for the server hosting experiment."),
                                        value: Provider.of<FlwtchState>(context, listen: false).cwtch.IsServersCompiled() && settings.isExperimentEnabled(ServerManagementExperiment),
                                        onChanged: Provider.of<FlwtchState>(context, listen: false).cwtch.IsServersCompiled()
                                            ? (bool value) {
                                                Provider.of<ServerListState>(context, listen: false).clear();
                                                if (value) {
                                                  settings.enableExperiment(ServerManagementExperiment);
                                                } else {
                                                  settings.disableExperiment(ServerManagementExperiment);
                                                }
                                                // Save Settings...
                                                saveSettings(context);
                                              }
                                            : null,
                                        activeTrackColor: settings.theme.defaultButtonColor,
                                        inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                                        secondary: Icon(CwtchIcons.dns_24px),
                                      )),
                                  SwitchListTile(
                                    title: Text(AppLocalizations.of(context)!.settingFileSharing),
                                    subtitle: Text(AppLocalizations.of(context)!.descriptionFileSharing),
                                    value: settings.isExperimentEnabled(FileSharingExperiment),
                                    onChanged: (bool value) {
                                      if (value) {
                                        if (checkDownloadDirectory(context, settings)) {
                                          settings.enableExperiment(FileSharingExperiment);
                                        } else {
                                          settings.enableExperiment(FileSharingExperiment);
                                          settings.disableExperiment(ImagePreviewsExperiment);
                                        }
                                      } else {
                                        settings.disableExperiment(FileSharingExperiment);
                                        settings.disableExperiment(ImagePreviewsExperiment);
                                      }
                                      saveSettings(context);
                                    },
                                    activeTrackColor: settings.theme.defaultButtonColor,
                                    inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                                    secondary: Icon(CwtchIcons.attached_file_3, color: settings.current().mainTextColor),
                                  ),
                                  Visibility(
                                    visible: settings.isExperimentEnabled(FileSharingExperiment),
                                    child: Column(children: [
                                      SwitchListTile(
                                        title: Text(AppLocalizations.of(context)!.settingImagePreviews),
                                        subtitle: Text(AppLocalizations.of(context)!.settingImagePreviewsDescription),
                                        value: settings.isExperimentEnabled(ImagePreviewsExperiment),
                                        onChanged: (bool value) {
                                          if (value) {
                                            if (checkDownloadDirectory(context, settings)) {
                                              settings.enableExperiment(ImagePreviewsExperiment);
                                            } else {
                                              settings.disableExperiment(ImagePreviewsExperiment);
                                            }
                                          } else {
                                            settings.disableExperiment(ImagePreviewsExperiment);
                                          }
                                          saveSettings(context);
                                        },
                                        activeTrackColor: settings.theme.defaultButtonColor,
                                        inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                                        secondary: Icon(Icons.photo, color: settings.current().mainTextColor),
                                      ),
                                      Visibility(
                                        visible: settings.isExperimentEnabled(ImagePreviewsExperiment) && !Platform.isAndroid,
                                        child: CwtchFolderPicker(
                                          testKey: Key("DownloadFolderPicker"),
                                          label: AppLocalizations.of(context)!.settingDownloadFolder,
                                          initialValue: settings.downloadPath,
                                          textStyle: settings.scaleFonts(defaultDropDownMenuItemTextStyle),
                                          description: AppLocalizations.of(context)!.fileSharingSettingsDownloadFolderDescription,
                                          tooltip: AppLocalizations.of(context)!.fileSharingSettingsDownloadFolderTooltip,
                                          onSave: (newVal) {
                                            settings.downloadPath = newVal;
                                            saveSettings(context);
                                          },
                                        ),
                                      ),
                                    ]),
                                  ),
                                  Visibility(
                                      visible: Provider.of<FlwtchState>(context, listen: false).cwtch.IsBlodeuweddSupported(),
                                      child: SwitchListTile(
                                        title: Text(AppLocalizations.of(context)!.blodeuweddExperimentEnable),
                                        subtitle: Provider.of<FlwtchState>(context, listen: false).cwtch.IsBlodeuweddSupported()
                                            ? Text(AppLocalizations.of(context)!.blodeuweddDescription)
                                            : Text(AppLocalizations.of(context)!.blodeuweddNotSupported),
                                        value: Provider.of<FlwtchState>(context, listen: false).cwtch.IsBlodeuweddSupported() && settings.isExperimentEnabled(BlodeuweddExperiment),
                                        onChanged: Provider.of<FlwtchState>(context, listen: false).cwtch.IsBlodeuweddSupported()
                                            ? (bool value) {
                                                if (value) {
                                                  settings.enableExperiment(BlodeuweddExperiment);
                                                } else {
                                                  settings.disableExperiment(BlodeuweddExperiment);
                                                }
                                                saveSettings(context);
                                              }
                                            : null,
                                        activeTrackColor: settings.theme.defaultButtonColor,
                                        inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                                        secondary: Icon(Icons.assistant, color: settings.current().mainTextColor),
                                      )),
                                  Visibility(
                                    visible: Provider.of<FlwtchState>(context, listen: false).cwtch.IsBlodeuweddSupported() && settings.isExperimentEnabled(BlodeuweddExperiment),
                                    child: CwtchFolderPicker(
                                      testKey: Key("DownloadFolderPicker"),
                                      label: AppLocalizations.of(context)!.settingDownloadFolder,
                                      initialValue: settings.blodeuweddPath,
                                      description: AppLocalizations.of(context)!.blodeuweddPath,
                                      tooltip: AppLocalizations.of(context)!.blodeuweddPath,
                                      onSave: (newVal) {
                                        settings.blodeuweddPath = newVal;
                                        saveSettings(context);
                                      },
                                    ),
                                  ),
                                ],
                              )),
                          Visibility(
                              visible: settings.experimentsEnabled,
                              child: SwitchListTile(
                                title: Text(AppLocalizations.of(context)!.enableExperimentClickableLinks),
                                subtitle: Text(AppLocalizations.of(context)!.experimentClickableLinksDescription),
                                value: settings.isExperimentEnabled(ClickableLinksExperiment),
                                onChanged: (bool value) {
                                  if (value) {
                                    settings.enableExperiment(ClickableLinksExperiment);
                                  } else {
                                    settings.disableExperiment(ClickableLinksExperiment);
                                  }
                                  saveSettings(context);
                                },
                                activeTrackColor: settings.theme.defaultButtonColor,
                                inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                                secondary: Icon(Icons.link, color: settings.current().mainTextColor),
                              )),
                          Visibility(
                              visible: settings.experimentsEnabled,
                              child: SwitchListTile(
                                title: Text(AppLocalizations.of(context)!.enableExperimentQRCode),
                                subtitle: Text(AppLocalizations.of(context)!.experimentQRCodeDescription),
                                value: settings.isExperimentEnabled(QRCodeExperiment),
                                onChanged: (bool value) {
                                  if (value) {
                                    settings.enableExperiment(QRCodeExperiment);
                                  } else {
                                    settings.disableExperiment(QRCodeExperiment);
                                  }
                                  saveSettings(context);
                                },
                                activeTrackColor: settings.theme.defaultButtonColor,
                                inactiveTrackColor: settings.theme.defaultButtonDisabledColor,
                                secondary: Icon(Icons.qr_code, color: settings.current().mainTextColor),
                              )),
                        ])))));
      });
    });
  }

  bool checkDownloadDirectory(context, settings) {
    bool showError = false;
    if (settings.downloadPath != "") {
    } else {
      // check if the default download path exists
      var path = Provider.of<FlwtchState>(context, listen: false).cwtch.defaultDownloadPath();
      if (path != null) {
        settings.downloadPath = path;
      } else {
        showError = true;
      }
    }

    if (!showError && Directory(settings.downloadPath).existsSync()) {
      return true;
    } else {
      final snackBar = SnackBar(
        content: Text(AppLocalizations.of(context)!.errorDownloadDirectoryDoesNotExist),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      return false;
    }
  }
}
