import 'package:cwtch/models/servers.dart';
import 'package:cwtch/themes/opaque.dart';
import 'package:cwtch/views/addeditservers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../cwtch_icons_icons.dart';
import '../errorHandler.dart';
import '../settings.dart';

class ServerRow extends StatefulWidget {
  @override
  _ServerRowState createState() => _ServerRowState();
}

class _ServerRowState extends State<ServerRow> {
  @override
  Widget build(BuildContext context) {
    var server = Provider.of<ServerInfoState>(context);
    return InkWell(
        enableFeedback: true,
        splashFactory: InkSplash.splashFactory,
        child: Ink(
            color: Colors.transparent,
            child: Container(
                child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Padding(
                  padding: const EdgeInsets.all(6.0), //border size
                  child: Row(children: [
                    Icon(CwtchIcons.dns_24px,
                        color: server.running ? Provider.of<Settings>(context).theme.portraitOnlineBorderColor : Provider.of<Settings>(context).theme.portraitOfflineBorderColor, size: 64),
                    Visibility(
                        visible: !server.running,
                        child: Icon(
                          CwtchIcons.negative_heart_24px,
                          color: Provider.of<Settings>(context).theme.portraitOfflineBorderColor,
                        )),
                  ])),
              Expanded(
                  child: Column(
                children: [
                  Text(
                    server.description,
                    semanticsLabel: server.description,
                    style: Provider.of<Settings>(context)
                        .scaleFonts(defaultFormLabelTextStyle)
                        .apply(color: server.running ? Provider.of<Settings>(context).theme.portraitOnlineBorderColor : Provider.of<Settings>(context).theme.portraitOfflineBorderColor),
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Visibility(
                      visible: !Provider.of<Settings>(context).streamerMode,
                      child: ExcludeSemantics(
                          child: Text(
                        server.onion,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        style: Provider.of<Settings>(context)
                            .scaleFonts(defaultFormLabelTextStyle)
                            .copyWith(color: server.running ? Provider.of<Settings>(context).theme.portraitOnlineBorderColor : Provider.of<Settings>(context).theme.portraitOfflineBorderColor),
                      )))
                ],
              )),

              // Copy server button
              IconButton(
                enableFeedback: true,
                splashRadius: Material.defaultSplashRadius / 2,
                tooltip: AppLocalizations.of(context)!.copyServerKeys,
                icon: Icon(CwtchIcons.address_copy, color: Provider.of<Settings>(context).current().mainTextColor),
                onPressed: () {
                  Clipboard.setData(new ClipboardData(text: server.serverBundle));
                  final snackBar = SnackBar(content: Text(AppLocalizations.of(context)!.copiedToClipboardNotification));
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                },
              ),

              // Edit button
              IconButton(
                enableFeedback: true,
                splashRadius: Material.defaultSplashRadius / 2,
                tooltip: AppLocalizations.of(context)!.editServerTitle,
                icon: Icon(Icons.create, color: Provider.of<Settings>(context).current().mainTextColor),
                onPressed: () {
                  _pushEditServer(server);
                },
              )
            ]))),
        onTap: () {
          _pushEditServer(server);
        });
  }

  void _pushEditServer(ServerInfoState server) {
    Provider.of<ErrorHandler>(context, listen: false).reset();
    Navigator.of(context).push(
        //MaterialPageRoute<void>(
        PageRouteBuilder(
      settings: RouteSettings(name: "serveraddedit"),
      pageBuilder: (bcontext, a1, a2) {
        return MultiProvider(
          providers: [ChangeNotifierProvider.value(value: server)],
          child: AddEditServerView(),
        );
      },
      transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
      transitionDuration: Duration(milliseconds: 200),
    ));
  }
}
