import 'package:cwtch/models/appstate.dart';
import 'package:cwtch/models/contactlist.dart';
import 'package:cwtch/models/profile.dart';
import 'package:cwtch/models/search.dart';
import 'package:flutter/material.dart';
import 'package:cwtch/views/addeditprofileview.dart';
import 'package:cwtch/views/contactsview.dart';
import 'package:cwtch/views/doublecolview.dart';
import 'package:cwtch/widgets/profileimage.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../main.dart';
import '../settings.dart';

class ProfileRow extends StatefulWidget {
  @override
  _ProfileRowState createState() => _ProfileRowState();
}

class _ProfileRowState extends State<ProfileRow> {
  @override
  Widget build(BuildContext context) {
    var profile = Provider.of<ProfileInfoState>(context);
    return InkWell(
      enableFeedback: true,
      splashFactory: InkSplash.splashFactory,
      child: Ink(
          color: Colors.transparent,
          child: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                  padding: const EdgeInsets.all(6.0), //border size
                  child: ProfileImage(
                      badgeCount: profile.unreadMessages,
                      badgeColor: Provider.of<Settings>(context).theme.portraitProfileBadgeColor,
                      badgeTextColor: Provider.of<Settings>(context).theme.portraitProfileBadgeTextColor,
                      disabled: !profile.enabled,
                      diameter: 64.0,
                      imagePath: Provider.of<Settings>(context).isExperimentEnabled(ImagePreviewsExperiment) ? profile.imagePath : profile.defaultImagePath,
                      border: profile.isOnline ? Provider.of<Settings>(context).theme.portraitOnlineBorderColor : Provider.of<Settings>(context).theme.portraitOfflineBorderColor)),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                      height: 18.0 * Provider.of<Settings>(context).fontScaling + 18.0,
                      clipBehavior: Clip.hardEdge,
                      decoration: BoxDecoration(),
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                      child: Text(
                        profile.nickname,
                        semanticsLabel: profile.nickname,
                        style: TextStyle(fontFamily: "Inter", fontSize: 18.0 * Provider.of<Settings>(context).fontScaling, fontWeight: FontWeight.bold),
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                      )),
                  Visibility(
                      visible: profile.getPrivateName().isNotEmpty,
                      child: Container(
                          height: 16.0 * Provider.of<Settings>(context).fontScaling + 16.0,
                          clipBehavior: Clip.hardEdge,
                          decoration: BoxDecoration(),
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0), // side padding because Container clip and text + italics has problems
                          child: Text(
                            profile.getPrivateName(),
                            semanticsLabel: profile.getPrivateName(),
                            style: TextStyle(fontFamily: "Inter", fontStyle: FontStyle.italic, fontSize: 16.0 * Provider.of<Settings>(context).fontScaling, fontWeight: FontWeight.bold),
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                          ))),
                  Visibility(
                      visible: !Provider.of<Settings>(context).streamerMode,
                      child: Container(
                          clipBehavior: Clip.hardEdge,
                          decoration: BoxDecoration(),
                          height: 14.0 * Provider.of<Settings>(context).fontScaling + 14.0,
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: ExcludeSemantics(
                              child: Text(
                            profile.onion,
                            softWrap: true,
                            style: TextStyle(
                                fontFamily: "RobotoMono",
                                fontSize: 14.0 * Provider.of<Settings>(context).fontScaling,
                                color: ((Provider.of<Settings>(context).theme.mainTextColor) as Color).withOpacity(0.8)),
                            overflow: TextOverflow.ellipsis,
                          ))))
                ],
              )),
              IconButton(
                enableFeedback: true,
                splashRadius: Material.defaultSplashRadius / 2,
                tooltip: AppLocalizations.of(context)!.editProfile + " " + profile.nickname,
                icon: Icon(Icons.create, color: Provider.of<Settings>(context).current().mainTextColor),
                onPressed: () {
                  _pushEditProfile(onion: profile.onion, displayName: profile.nickname, profileImage: profile.imagePath, encrypted: profile.isEncrypted);
                },
              )
            ],
          ))),
      onTap: () {
        setState(() {
          var appState = Provider.of<AppState>(context, listen: false);
          appState.selectedProfile = profile.onion;
          appState.selectedConversation = null;

          _pushContactList(profile, appState.isLandscape(context)); //orientation == Orientation.landscape);
        });
      },
    );
  }

  void _pushContactList(ProfileInfoState profile, bool isLandscape) {
    Navigator.of(context).push(
      PageRouteBuilder(
        settings: RouteSettings(name: "conversations"),
        pageBuilder: (c, a1, a2) {
          return OrientationBuilder(builder: (orientationBuilderContext, orientation) {
            return MultiProvider(
                providers: [
                  ChangeNotifierProvider<ProfileInfoState>.value(value: profile),
                  ChangeNotifierProvider<ContactListState>.value(value: profile.contactList),
                  ChangeNotifierProvider<SearchState>.value(value: profile.searchState)
                ],
                builder: (innercontext, widget) {
                  var appState = Provider.of<AppState>(context);
                  var settings = Provider.of<Settings>(context);
                  return settings.uiColumns(appState.isLandscape(innercontext)).length > 1 ? DoubleColumnView() : ContactsView();
                });
          });
        },
        transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 200),
      ),
    );
  }

  void _pushEditProfile({onion = "", displayName = "", profileImage = "", encrypted = true}) {
    Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (bcontext, a1, a2) {
          var profile = Provider.of<FlwtchState>(bcontext).profs.getProfile(onion)!;
          return MultiProvider(
            providers: [
              ChangeNotifierProvider<ProfileInfoState>.value(
                value: profile,
              ),
            ],
            builder: (context, widget) => AddEditProfileView(),
          );
        },
        transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 200),
      ),
    );
  }
}
