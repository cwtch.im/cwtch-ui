import 'dart:io';

import 'package:cwtch/models/appstate.dart';
import 'package:cwtch/models/contact.dart';
import 'package:cwtch/models/contactlist.dart';
import 'package:cwtch/models/profile.dart';
import 'package:cwtch/models/redaction.dart';
import 'package:cwtch/models/search.dart';
import 'package:cwtch/themes/opaque.dart';
import 'package:cwtch/views/contactsview.dart';
import 'package:flutter/material.dart';
import 'package:cwtch/widgets/profileimage.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../main.dart';
import '../models/message.dart';
import '../settings.dart';

class ContactRow extends StatefulWidget {
  int? messageIndex;

  ContactRow({this.messageIndex});
  @override
  _ContactRowState createState() => _ContactRowState();
}

class _ContactRowState extends State<ContactRow> {
  bool isHover = false;
  Message? cachedMessage;

  @override
  Widget build(BuildContext context) {
    var contact = Provider.of<ContactInfoState>(context);

    if (widget.messageIndex != null && this.cachedMessage == null) {
      messageHandler(context, Provider.of<ProfileInfoState>(context, listen: false).onion, contact.identifier, ByIndex(widget.messageIndex!)).then((value) {
        setState(() {
          this.cachedMessage = value;
        });
      });
    }

    // Only groups have a sync status
    Widget? syncStatus;
    if (contact.isGroup) {
      syncStatus = Visibility(
          visible: contact.isGroup && contact.status == "Authenticated",
          child: LinearProgressIndicator(
            color: Provider.of<Settings>(context).theme.defaultButtonActiveColor,
            backgroundColor: Provider.of<Settings>(context).theme.defaultButtonDisabledColor,
            value: Provider.of<ProfileInfoState>(context).serverList.getServer(contact.server ?? "")?.syncProgress,
          ));
    }

    bool selected = Provider.of<AppState>(context).selectedConversation == contact.identifier;
    if (selected && widget.messageIndex != null) {
      if (selected && widget.messageIndex == Provider.of<SearchState>(context).selectedSearchMessage) {
        selected = true;
      } else {
        selected = false;
      }
    }
    return InkWell(
      enableFeedback: true,
      splashFactory: InkSplash.splashFactory,
      child: Ink(
          color: selected ? (Provider.of<Settings>(context).theme.backgroundHilightElementColor as Color).withOpacity(0.8) : Colors.transparent,
          child: Container(
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.center, children: [
            Padding(
                padding: const EdgeInsets.all(6.0), //border size
                child: ProfileImage(
                  badgeCount: contact.unreadMessages,
                  badgeColor: Provider.of<Settings>(context).theme.portraitContactBadgeColor,
                  badgeTextColor: Provider.of<Settings>(context).theme.portraitContactBadgeTextColor,
                  diameter: 64.0,
                  imagePath: Provider.of<Settings>(context).isExperimentEnabled(ImagePreviewsExperiment) ? contact.imagePath : contact.defaultImagePath,
                  disabled: !contact.isOnline(),
                  border: contact.getBorderColor(Provider.of<Settings>(context).theme),
                )),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.all(6.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                            clipBehavior: Clip.hardEdge,
                            height: Provider.of<Settings>(context).fontScaling * 14.0 + 5.0,
                            decoration: BoxDecoration(),
                            child: Text(
                              contact.augmentedNickname(context).trim() + (contact.messageDraft.isEmpty() ? "" : "*"),
                              style: TextStyle(
                                  fontSize: Provider.of<Settings>(context).fontScaling * 14.0,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.bold,
                                  color: contact.isBlocked
                                      ? Provider.of<Settings>(context).theme.portraitBlockedTextColor
                                      : Provider.of<Settings>(context).theme.mainTextColor), //Provider.of<FlwtchState>(context).biggerFont,
                              softWrap: true,
                              overflow: TextOverflow.clip,
                              maxLines: 1,
                            )),
                        syncStatus ??
                            SizedBox(
                              width: 0,
                              height: 0,
                            ),
                        // we need to ignore the child widget in this context, otherwise gesture events will flow down...
                        IgnorePointer(
                            ignoring: true,
                            child: Visibility(
                              visible: this.cachedMessage != null,
                              maintainSize: false,
                              maintainInteractivity: false,
                              maintainSemantics: false,
                              maintainState: false,
                              child: this.cachedMessage == null ? CircularProgressIndicator() : this.cachedMessage!.getPreviewWidget(context),
                            )),
                        Container(
                            padding: EdgeInsets.all(0),
                            height: contact.isInvitation ? Provider.of<Settings>(context).fontScaling * 14.0 + 35.0 : Provider.of<Settings>(context).fontScaling * 14.0 + 5.0,
                            child: contact.isInvitation == true
                                ? FittedBox(
                                    fit: BoxFit.scaleDown,
                                    child: Wrap(children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.all(2),
                                          child: TextButton.icon(
                                            label: Text(
                                              AppLocalizations.of(context)!.tooltipAcceptContactRequest,
                                              style: Provider.of<Settings>(context).scaleFonts(defaultTextButtonStyle),
                                            ),
                                            icon: Icon(
                                              Icons.favorite,
                                              size: 16,
                                              color: Provider.of<Settings>(context).theme.mainTextColor,
                                            ),
                                            onPressed: _btnApprove,
                                          )),
                                      Padding(
                                          padding: EdgeInsets.all(2),
                                          child: TextButton.icon(
                                            label: Text(
                                              AppLocalizations.of(context)!.tooltipRejectContactRequest,
                                              style: Provider.of<Settings>(context).scaleFonts(defaultTextButtonStyle),
                                            ),
                                            style: ButtonStyle(
                                                backgroundColor: MaterialStateProperty.all(Provider.of<Settings>(context).theme.backgroundPaneColor),
                                                foregroundColor: MaterialStateProperty.all(Provider.of<Settings>(context).theme.mainTextColor)),
                                            icon: Icon(Icons.delete, size: 16, color: Provider.of<Settings>(context).theme.mainTextColor),
                                            onPressed: _btnReject,
                                          ))
                                    ]))
                                : (contact.isBlocked
                                    ? IconButton(
                                        padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 0.0),
                                        splashRadius: Material.defaultSplashRadius / 2,
                                        iconSize: 16,
                                        icon: Icon(Icons.block, color: Provider.of<Settings>(context).theme.mainTextColor),
                                        onPressed: null,
                                      )
                                    : Text(prettyDateString(context, widget.messageIndex == null ? contact.lastMessageSentTime : (this.cachedMessage?.getMetadata().timestamp ?? DateTime.now())),
                                        style: Provider.of<Settings>(context).scaleFonts(TextStyle(
                                          fontSize: 12.0,
                                          fontFamily: "Inter",
                                        ))))),
                        Visibility(
                            visible: !Provider.of<Settings>(context).streamerMode,
                            child: Container(
                                padding: EdgeInsets.all(0),
                                height: Provider.of<Settings>(context).fontScaling * 13.0 + 5.0,
                                child: FittedBox(
                                    fit: BoxFit.scaleDown,
                                    child: Text(
                                      contact.onion,
                                      overflow: TextOverflow.ellipsis,
                                      style: Provider.of<Settings>(context).scaleFonts(TextStyle(
                                          fontSize: 13.0,
                                          fontFamily: "RobotoMono",
                                          color: ((contact.isBlocked ? Provider.of<Settings>(context).theme.portraitBlockedTextColor : Provider.of<Settings>(context).theme.mainTextColor) as Color)
                                              .withOpacity(0.8))),
                                    )))),
                      ],
                    ))),
            Visibility(
                // only allow pinning for non-blocked and accepted conversations,
                visible: contact.isAccepted() && (Platform.isAndroid || (!Platform.isAndroid && isHover) || contact.pinned),
                child: IconButton(
                  tooltip: contact.pinned ? AppLocalizations.of(context)!.tooltipUnpinConversation : AppLocalizations.of(context)!.tooltipPinConversation,
                  icon: Icon(
                    contact.pinned ? Icons.push_pin : Icons.push_pin_outlined,
                    color: Provider.of<Settings>(context).theme.mainTextColor,
                  ),
                  onPressed: () {
                    if (contact.pinned) {
                      contact.unpin(context);
                    } else {
                      contact.pin(context);
                    }
                    Provider.of<ProfileInfoState>(context, listen: false).resortContacts();
                  },
                ))
          ]))),
      onTap: () {
        setState(() {
          selectConversation(context, contact.identifier, widget.messageIndex);
        });
      },
      onHover: (hover) {
        if (isHover != hover) {
          setState(() {
            isHover = hover;
          });
        }
      },
    );
  }

  void _btnApprove() {
    Provider.of<ContactInfoState>(context, listen: false).accepted = true;
    Provider.of<FlwtchState>(context, listen: false)
        .cwtch
        .AcceptContact(Provider.of<ContactInfoState>(context, listen: false).profileOnion, Provider.of<ContactInfoState>(context, listen: false).identifier);
  }

  void _btnReject() {
    Provider.of<ContactInfoState>(context, listen: false).blocked = true;
    ContactInfoState contact = Provider.of<ContactInfoState>(context, listen: false);
    if (contact.isGroup == true) {
      // FIXME This flow is incorrect. Groups never just show up on the contact list anymore
      Provider.of<ProfileInfoState>(context, listen: false).removeContact(contact.onion);
    } else {
      Provider.of<FlwtchState>(context, listen: false).cwtch.BlockContact(Provider.of<ContactInfoState>(context, listen: false).profileOnion, contact.identifier);
    }
  }
}
