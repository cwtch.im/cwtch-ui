import 'package:cwtch/cwtch_icons_icons.dart';
import 'package:cwtch/main.dart';
import 'package:cwtch/models/contact.dart';
import 'package:cwtch/models/profile.dart';
import 'package:cwtch/settings.dart';
import 'package:cwtch/themes/opaque.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

enum ConversationOptionsMenu { settings, disconnect, manage_shares }

class ConversationOptions extends StatefulWidget {
  final void Function() SelectSettings;
  final void Function() ManageFiles;

  ConversationOptions(this.SelectSettings, this.ManageFiles);
  @override
  _ConversationOptionsState createState() => _ConversationOptionsState();
}

class _ConversationOptionsState extends State<ConversationOptions> {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<ConversationOptionsMenu>(
        icon: Provider.of<ContactInfoState>(context, listen: false).isGroup == true ? Icon(CwtchIcons.group_settings_24px) : Icon(CwtchIcons.peer_settings_24px),
        iconSize: 24,
        tooltip: AppLocalizations.of(context)!.availabilityStatusTooltip,
        splashRadius: Material.defaultSplashRadius / 2,
        onSelected: (ConversationOptionsMenu item) {
          switch (item) {
            case ConversationOptionsMenu.settings:
              widget.SelectSettings();
              break;
            case ConversationOptionsMenu.disconnect:
              if (Provider.of<ContactInfoState>(context, listen: false).isGroup) {
                Provider.of<FlwtchState>(context, listen: false)
                    .cwtch
                    .DisconnectFromServer(Provider.of<ProfileInfoState>(context, listen: false).onion, Provider.of<ContactInfoState>(context, listen: false).server!);
              } else {
                Provider.of<FlwtchState>(context, listen: false)
                    .cwtch
                    .DisconnectFromPeer(Provider.of<ProfileInfoState>(context, listen: false).onion, Provider.of<ContactInfoState>(context, listen: false).onion);
              }
              // reset the disconnect button to allow for immediate connection...
              Provider.of<ContactInfoState>(context, listen: false).lastRetryTime = DateTime.now().subtract(Duration(minutes: 2));
              Provider.of<ContactInfoState>(context, listen: false).contactEvents.add(ContactEvent("Disconnect from Peer"));
              break;
            case ConversationOptionsMenu.manage_shares:
              widget.ManageFiles();
              break;
          }
        },
        itemBuilder: (context) {
          return <PopupMenuEntry<ConversationOptionsMenu>>[
            PopupMenuItem<ConversationOptionsMenu>(
              value: ConversationOptionsMenu.settings,
              enabled: true,
              child: Row(children: [
                Icon(
                  CwtchIcons.account_circle_24px,
                  color: Colors.white,
                ),
                Expanded(
                    child:
                        Text(AppLocalizations.of(context)!.conversationSettings, textAlign: TextAlign.right, style: Provider.of<Settings>(context, listen: false).scaleFonts(defaultTextButtonStyle)))
              ]),
            ),
            PopupMenuItem<ConversationOptionsMenu>(
                value: ConversationOptionsMenu.disconnect,
                enabled: Provider.of<ContactInfoState>(context, listen: false).isOnline(),
                child: Tooltip(
                  message: AppLocalizations.of(context)!.contactDisconnect,
                  child: Row(children: [
                    Icon(
                      CwtchIcons.disconnect_from_contact,
                      color: Colors.white,
                    ),
                    Expanded(
                        child: Text(AppLocalizations.of(context)!.contactDisconnectSummary,
                            textAlign: TextAlign.right, style: Provider.of<Settings>(context, listen: false).scaleFonts(defaultTextButtonStyle)))
                  ]),
                )),
            PopupMenuItem<ConversationOptionsMenu>(
              value: ConversationOptionsMenu.manage_shares,
              enabled: Provider.of<Settings>(context, listen: false).isExperimentEnabled(FileSharingExperiment),
              child: Row(children: [
                Icon(
                  CwtchIcons.manage_files,
                  color: Colors.white,
                ),
                Expanded(
                    child: Text(AppLocalizations.of(context)!.manageSharedFiles, textAlign: TextAlign.right, style: Provider.of<Settings>(context, listen: false).scaleFonts(defaultTextButtonStyle)))
              ]),
            ),
          ];
        });
  }
}
