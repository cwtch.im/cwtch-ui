echo "`n** Don't forget to run clean.ps1 once if this is a new commit **`n"

.\windows\build\save-build-vars.ps1
.\windows\build\load-build-vars.ps1

$Env:releasedir = "build\\windows\\x64\\runner\\Release\\"

flutter build windows --dart-define BUILD_VER=$Env:version --dart-define BUILD_DATE=$Env:commitdate

.\windows\build\package-deps.ps1