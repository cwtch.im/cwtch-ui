.\windows\build\load-build-vars.ps1

$env:Path += ";C:\Program Files (x86)\Windows Kits\10\App Certification Kit\"

signtool sign  /i "HARICA EV Code Signing ECC SubCA R1" /tr http://timestamp.digicert.com /td SHA256 /fd SHA256 $Env:releasedir\cwtch.exe
signtool sign  /i "HARICA EV Code Signing ECC SubCA R1" /tr http://timestamp.digicert.com /td SHA256 /fd SHA256 $Env:releasedir\libCwtch.dll
signtool sign  /i "HARICA EV Code Signing ECC SubCA R1" /tr http://timestamp.digicert.com /td SHA256 /fd SHA256 $Env:releasedir\flutter_windows.dll

makensis windows\nsis\cwtch-installer.nsi
rm cwtch-installer-.exe
move windows\nsis\cwtch-installer.exe cwtch-installer-$Env:version.exe

signtool sign  /i "HARICA EV Code Signing ECC SubCA R1" /tr http://timestamp.digicert.com /td SHA256 /fd SHA256 cwtch-installer-$Env:version.exe