copy windows\libCwtch.dll $Env:releasedir

# flutter hasn't worked out it's packaging of required dll's so we have to resort to this manual nonsense
# https://github.com/google/flutter-desktop-embedding/issues/587
# https://github.com/flutter/flutter/issues/53167
copy windows\runner\resources\knot_128.ico $Env:releasedir\cwtch.ico
copy 'C:\Windows\System32\vcruntime140.dll' $Env:releasedir
copy 'C:\Windows\System32\vcruntime140_1.dll' $Env:releasedir
copy 'C:\Windows\System32\msvcp140.dll' $Env:releasedir

copy README.md $Env:releasedir\
copy windows\*.bat $Env:releasedir\
powershell -command "Expand-Archive -Path tor.zip -DestinationPath $Env:releasedir\Tor"