import 'dart:convert';
import 'package:cwtch/main.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

StepDefinitionGeneric TakeScreenshot() {
  return then<FlutterWorld>(
    RegExp(r'I take a screenshot'),
    (context) async {
      try {
        final bytes = await context.world.appDriver.screenshot();
        final screenshotData = base64Encode(bytes);
        print("EMBEDDING SCREENSHOT....");
        print("$screenshotData");
        context.world.attach(screenshotData, 'image/png', 'And I take a screenshot');
      } catch (e, st) {
        print("FAILED TO EMBED??? $e $st");
        context.world.attach('Failed to take screenshot\n$e\n$st', 'text/plain');
      }
    },
  );
}

StepDefinitionGeneric ScrollToButton() {
  return given1<String, FlutterWorld>(
    RegExp(r'I scroll to the {string} (?:button|element|label|icon|field|text|widget)'),
        (input1, context) async {
      final finder = context.world.appDriver.findByDescendant(context.world.appDriver.findBy(Flwtch, FindType.type), context.world.appDriver.findBy(input1, FindType.key), firstMatchOnly: true);
      print("have a finder for button $input1");
      print(finder);
      print("scrolling to...");
      await context.world.appDriver.scrollIntoView(finder);
      await context.world.appDriver.waitForAppToSettle();
    },
  );
}