@env:persist
Feature: Basic Profile Management
    Scenario: Error on Creating a Profile without a Display Name
        Given I wait until the widget with type 'ProfileMgrView' is present
        And I tap a button with tooltip "Add new profile"
        And I tap the "addNewProfileActual" button
        And I wait until the text 'Display Name' is present
        And I expect the text 'New Password' to be present
        And I expect the text 'Please enter a display name' to be absent
        Then I tap the "button" widget with label "Add new profile"
        And I expect the text 'Please enter a display name' to be present
        And I take a screenshot
        And I wait for 5 seconds

    Scenario: Create Unencrypted Profile
        Given I wait until the widget with type 'ProfileMgrView' is present
        And I tap a button with tooltip "Add new profile"
        And I tap the "addNewProfileActual" button
        And I wait until the text 'Display Name' is present
        And I expect the text 'New Password' to be present
        # And I take a screenshot
        Then I tap the "passwordCheckBox" widget
        And I expect the text 'New Password' to be absent within 2 seconds
        # And I take a screenshot
        Then I fill the "displayNameFormElement" field with "Alice (Unencrypted)"
        Then I tap the "button" widget with label "Add new profile"
        And I expect a "ProfileRow" widget with text "Alice (Unencrypted)"
        # And I take a screenshot
        Then I tap the "ProfileRow" widget with label "Alice (Unencrypted)"
        And I expect the text "Alice (Unencrypted) » Conversations" to be present
        # And I take a screenshot

    Scenario: Load Unencrypted Profile
        Given I wait until the widget with type 'ProfileMgrView' is present
        Then I expect a "ProfileRow" widget with text "Alice (Unencrypted)"
        # This test is too short...if the test finishes before flutter has finished initializing then
        # the framework gets very confused...
        And I wait for 2 seconds

    Scenario: Create Encrypted Profile
        Given I wait until the widget with type 'ProfileMgrView' is present
        And I tap a button with tooltip "Add new profile"
        And I tap the "addNewProfileActual" button
        And I wait until the text 'Display Name' is present
        And I expect the text 'New Password' to be present
        # And I take a screenshot
        Then I fill the "displayNameFormElement" field with "Alice (Encrypted)"
        Then I fill the "passwordFormElement" field with "password1"
        Then I fill the "confirmPasswordFormElement" field with "password1"
        # And I take a screenshot
        Then I tap the "button" widget with label "Add new profile"
        And I expect a "ProfileRow" widget with text "Alice (Encrypted)"
        # And I take a screenshot
        Then I tap the "ProfileRow" widget with label "Alice (Encrypted)"
        And I expect the text 'Alice (Encrypted) » Conversations' to be present
        # And I take a screenshot

    Scenario: Load an Encrypted Profile by Unlocking it with a Password
        Given I wait until the widget with type 'ProfileMgrView' is present
        Then I expect the text 'Enter a password to view your profiles' to be absent
        And I tap a button with tooltip "Unlock encrypted profiles by entering their password."
        Then I expect the text 'Enter a password to view your profiles' to be present
        When I fill the "unlockPasswordProfileElement" field with "password1"
        And I tap the "button" widget with label "Unlock"
        Then I expect a "ProfileRow" widget with text "Alice (Encrypted)"

    Scenario: Load an Encrypted Profile by Unlocking it with a Password and Change the Name
        Given I wait until the widget with type 'ProfileMgrView' is present
        Then I expect the text 'Enter a password to view your profiles' to be absent
        And I tap a button with tooltip "Unlock encrypted profiles by entering their password."
        Then I expect the text 'Enter a password to view your profiles' to be present
        When I fill the "unlockPasswordProfileElement" field with "password1"
        And I tap the "button" widget with label "Unlock"
        Then I expect a "ProfileRow" widget with text "Alice (Encrypted)"
        When I tap the "IconButton" widget with tooltip "Edit Profile Alice (Encrypted)"
        Then I expect the text 'Display Name' to be present
        Then I fill the "displayNameFormElement" field with "Carol (Encrypted)"
        And I scroll to the "createOrSaveProfileBtn" button
        And I tap the "createOrSaveProfileBtn" button
        And I wait until the widget with type 'ProfileMgrView' is present
        Then I expect a "ProfileRow" widget with text "Carol (Encrypted)"

     Scenario: Delete an Encrypted Profile
         Given I wait until the widget with type 'ProfileMgrView' is present
         Then I expect the text 'Enter a password to view your profiles' to be absent
         And I tap a button with tooltip "Unlock encrypted profiles by entering their password."
         Then I expect the text 'Enter a password to view your profiles' to be present
         When I fill the "unlockPasswordProfileElement" field with "password1"
         And I tap the "button" widget with label "Unlock"
         Then I expect a "ProfileRow" widget with text "Carol (Encrypted)"
         # And I take a screenshot
         When I tap the "IconButton" widget with tooltip "Edit Profile Carol (Encrypted)"
         Then I expect the text 'Display Name' to be present
         Then I fill the "currentPasswordFormElement" field with "password1"
         And I tap the widget that contains the text "Delete"
         Then I wait for 2 seconds
         When I tap the "button" widget with label "Really Delete Profile"
         And I wait until the widget with type 'ProfileMgrView' is present
         Then I expect a "ProfileRow" widget with text "Carol (Encrypted)" to be absent
