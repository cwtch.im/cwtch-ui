
if (Test-Path -Path "Tor\") {
    rm -r Tor
}

Invoke-WebRequest -Uri https://git.openprivacy.ca/openprivacy/buildfiles/raw/branch/master/tor/tor-0.4.8.14-windows-x86_64.zip -OutFile tor.zip

if ((Get-FileHash tor.zip -Algorithm sha512).Hash -ne '85643ccedf5cbb91c4ac40a6c1aeab2ad541baa1be7725748941f9662dc91e4221daa0b57d6e4e59b3b034fc1b56e257ac3fc6aefa801e4d6d02b7942b8959f8' ) { Write-Error 'tor.zip sha512sum mismatch' }

Expand-Archive -Path tor.zip -DestinationPath Tor
