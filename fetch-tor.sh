#!/bin/sh

cd linux
rm -r tor
rm -r Tor
curl --fail https://git.openprivacy.ca/openprivacy/buildfiles/raw/branch/master/tor/tor-0.4.8.14-linux-x86_64.tar.gz --output tor.tar.gz
tar -xzf tor.tar.gz
mv tor Tor
cd ..

mkdir -p android/app/src/main/jniLibs/arm64-v8a
curl --fail https://git.openprivacy.ca/openprivacy/buildfiles/raw/branch/master/tor/tor-0.4.8.14-android-arm64 --output android/app/src/main/jniLibs/arm64-v8a/libtor.so
chmod a+x android/app/src/main/jniLibs/arm64-v8a/libtor.so

mkdir -p android/app/src/main/jniLibs/armeabi-v7a
curl --fail https://git.openprivacy.ca/openprivacy/buildfiles/raw/branch/master/tor/tor-0.4.8.14-android-armv7 --output android/app/src/main/jniLibs/armeabi-v7a/libtor.so
chmod a+x android/app/src/main/jniLibs/armeabi-v7a/libtor.so
