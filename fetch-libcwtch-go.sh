#!/bin/sh

VERSION=`cat LIBCWTCH-GO.version`
echo $VERSION

curl --fail  https://build.openprivacy.ca/files/libCwtch-autobindings-$VERSION/android/cwtch.aar --output android/app/cwtch/cwtch.aar
# FIXME...at some point we need to support different linux architectures...for now rely on existing expectations and rename x64 lib
curl --fail  https://build.openprivacy.ca/files/libCwtch-autobindings-$VERSION/linux/amd64/libCwtch.so --output linux/libCwtch.so
