#!/bin/sh

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

cd $SCRIPTPATH

exec env LD_LIBRARY_PATH=PREFIX/lib/cwtch/ PREFIX/lib/cwtch/cwtch
